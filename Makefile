BOOK=EITv2book.pdf
all: $(BOOK)

SUBDIRS = \
   Chap01-Introduction                  \
   Chap02-Tissue-Electrical-Properties  \
   Chap03-Electronics-Hardware          \
   Chap04-Forward-Problem               \
   Chap05-Inverse-Problem               \
   Chap06-Dbar-Algorithms               \
   Chap07-Interpretation                \
   Chap08-Lung-Function                 \
   Chap09-Venilation-Monitoring         \
   Chap10-Hemodynamics-EIT              \
   Chap11-Brain+Nerves                  \
   Chap12-Cancer                        \
   Chap13-Other                         \
   Chap14-MIT                           \
   Chap15-MREIT                         \
   Chap16-GeophysicalEIT                \
   Chap17-Process-Tomography            \
   Chap18-Veterinary-Applications       \
   Chap19-Devices-History-Conferences   \
   Chap20-Symbols+Terminology

.PHONY: $(SUBDIRS)

$(BOOK): $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

$(BOOK): figincludes.tex

figincludes.tex: Makefile
	find Chap*-* -type d -name fig-\* -print0 | sort -z --version-sort | xargs -0 -I 'XXX' echo \
            "\\def\\FIGDIR{###XXX}" \
            "\\def\\CURRCHAP{XXX###}" \
            "\\input{XXX/code+data.tex}" \
            "\\label{fig:XXX}" \
             > $@
	perl -i -pe's{/fig-[^/]*###}{};s{###Chap\d\d-[^/]*/}{};' $@

$(BOOK): %.pdf: %.tex
	latexmk -pdf $<


old:
	latexmk -f -pdf -halt-on-error -file-line-error $<

#latexmk -f -pdf -pdflatex "pdflatex -halt-on-error -file-line-error $<"
