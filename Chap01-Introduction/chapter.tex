\ifx\CURRCHAP\undefined \def\BUILDCHAPTERALONE{TRUE} \fi
\ifdefined\BUILDCHAPTERALONE
 \def\CURRCHAP{.} \input{../includes.tex}
 \begin{document}
 \setcounter{chapter}{01}\addtocounter{chapter}{-1} 
\fi

\chapter{%
Electrical Impedance Tomography
}
\label{ch01:main}
\chapterauthor{ Andy Adler }
              { Andy Adler }
\chapteraffil{
Systems and Computer Engineering, Carleton University, 
     Ottawa, Canada
}

\section{Introduction}

Electrical Impedance Tomography (EIT) is a technique to create tomographic images
of the electrical properties of tissue within a body based on electrical
impedance\index{impedance} measurements at body-surface electrodes\index{electrode}.
%\Figref{
%\FIGCAPTION{fig:ch01:EIToverview1}{\CURRCHAP/fig-EIT-example1}
\Figref{fig:ch01:EITbaby}
illustrates the concept: electrodes\index{electrode} are placed onto the chest, and images and
waveforms\index{waveform} of air and blood\index{blood} movement calculated.
The term \emph{Tomography} refers to imaging the volume within a body
using penetrating energy from outside the body. 
Tomographic modalities are typically named after
the energy used or its interaction with the body; 
EIT is thus named since it
uses measurements of electrical \emph{impedance\index{impedance}}.

\begin{figure}[h]\centering
\includegraphics[width=0.8\columnwidth]{\CURRCHAP/fig-baby-breathing/EIT-baby.pdf}
\FIGCAPTION{fig:ch01:EITbaby}{\CURRCHAP/fig-baby-breathing}
\end{figure}


 This is the second edition
of this book, following up the 2004 edition \cite{Holder2004Electrical} almost
twenty years ago.
 The basics
of the technology and its applications have not changed;
what has changed is that EIT
is finally being used, clinically, to make decisions about the patient
therapy, such as for patients of the COVID-19\index{COVID-19} pandemic during the writing of this book
\cite{vanderZee2020Electrical}.
Commercial and research systems are available.
EIT is accepted as a scientific tool,
for example, to help understand the unique physiology of a baby's first cry
\cite{Tingay2021Imaging}.
Technical developments now also focus on calculating
robust and clinically-useful parameters.



\section{Overview}


Biomedical EIT is an inter-disciplinary field.
Some clinical terminology and concepts in application chapters may be
unfamiliar to readers with maths or physics backgrounds, and
the reconstruction\index{reconstruction} algorithms or
instrumentation chapters may be difficult to follow for clinical readers.
The next chapter, \ref{ch0B:main},
is
intended as a non-technical introduction to technology and concepts in
 biomedical electrical
impedance\index{impedance} tomography\index{tomography}.

\begin{figure}[h]\centering
\includegraphics[width=\textwidth]
  {\CURRCHAP/fig-steps-overview/secEIT-overview.pdf}
\FIGCAPTION{fig:ch01:overview}{\CURRCHAP/fig-steps-overview}
\end{figure}

The second part of this book (Chapters \ref{ch02:main} -- \ref{ch07:main})
describes the story of an EIT measurement from tissue properties, through
measurement, imaging and interpretation, as illustrated in 
\figref{fig:ch01:overview}.
We have found it useful to categorize the various components of
EIT analysis through this type of processing pathway
\cite{Adler2017Electrical, Frerichs2004Distribution}. The input is
a physiological or anatomical feature of interest which leads to
changes in tissue impedance\index{impedance}, such as the movement of non-conductive air
in the lungs, or the presence a tumour surrounded by a conductive network
of blood\index{blood} vessels. The output is a functional image or measure which
is scientifically or clinically relevant. If we wish to know if an intervention has 
improved air flow to a patient, we need to calculate flow from the EIT
image, and compare it before and after the treatment.
After describing the tissue impedance\index{impedance} properties
(chapter \ref{ch02:main}),
we review EIT electronics hardware and signal acquisition 
(chapter \ref{ch03:main}).
We next need to understand how current propagates in the body
(chapter \ref{ch04:main}) in order to build a model
from which we can calculate tomographic images
(chapter \ref{ch05:main}), as well as 
a new set of ``D-bar\index{D-bar}'' approaches for image reconstruction\index{reconstruction} 
(chapter \ref{ch06:main}).
Finally, from these images, clinically-relevant functional parameters and measures
are calculated 
(chapter \ref{ch07:main}).
These steps are reviewed in more detail below
(\secref{sec:ch01:WorkingPrincipleEIT}).



The next part of this book (chapters \ref{ch08:main} -- \ref{ch13:main})
reviews clinical applications of EIT. EIT has seen the most widespread
application for imaging of the lungs
(chapter \ref{ch08:main}), and is currently seeing clinical application
for monitoring of ventilation in intensive care and surgical patients
(chapter \ref{ch09:main}). When making EIT recordings on the chest, useful
information about blood flow (perfusion\index{perfusion}) is also available, and EIT
shows promise for measuring hemodynamic parameters (chapter \ref{ch10:main}).
The next chapter (\ref{ch11:main}) describes the application of EIT for imaging
the brain\index{brain} and nerves, followed by a description of its use for
imaging of cancerous\index{cancerous} regions (chapter \ref{ch12:main}). EIT has also
been evaluated for use in many other applications (\ref{ch13:main}).
In addition to human medicine, EIT is also seeing use in
veterinary applications (chapter \ref{ch18:main}).

The next part describes many allied technologies which are relevant to
biomedical EIT. Tissue electrical properties can be measured magnetically,
through Magnetic Induction Tomography\index{MIT}
(MIT\index{MIT}, chapter \ref{ch14:main}).
Very high resolution images of tissue impedance\index{impedance} can be created using
magnetic resonance imaging in MREIT\index{MREIT} (chapter \ref{ch15:main}).
The principles underlying medical EIT are also relevant to 
other disciplines. Geophysical prospecting with
Electrical Resistivity Tomography\index{ERT}
(ERT) was the first tomographic imaging modality, starting
in 1911 \cite{Allaud1977Schlumberger}, and, in many ways, geophysical ERT\index{ERT} is 
more advanced than biomedical EIT (chapter \ref{ch16:main}).
Another important use of ERT is in process tomography\index{tomography} for 
monitoring of such processes as pipe flows and industrial mixing
(chapter \ref{ch17:main}).
Finally, we briefly describe the EIT devices available (both historically
important and currently used systems)
and details about conferences and proceedings
 in chapter \ref{ch19:main}.
% and provide a list of terminology and a reference to conference in EIT (chapter \ref{ch20:main}).


\section{EIT image generation and interpretation}
\label{sec:ch01:WorkingPrincipleEIT}

\begin{figure}[!ht]\centering
\includegraphics[width=0.7\columnwidth,page=1]{\CURRCHAP/fig-EIT-example1/EIT-overview.pdf}
\FIGCAPTION{fig:ch01:EIToverview1}{\CURRCHAP/fig-EIT-example1}
\end{figure}

EIT generates images 
with a relatively low resolution of about 10\% of the diameter of the
surrounding surface electrodes\index{electrode}, but a
high temporal resolution, of a few tens of ms.
It uses physically small hardware with body surface electrodes\index{electrode} that 
are less cumbersome than many other medical imaging devices.
\Figref{fig:ch01:EIToverview1} illustrates
EIT data collection.
These figures use data from a health
newborn\cite{Heinrich2006Body} with 16 ECG-type\index{ECG}
electrodes\index{electrode}. 
A volume model of the tissue properties
is used to reconstruct EIT images.
We illustrate the heart activity, 
showing the electrocardiogram (ECG)
signal (for reference only) and the
ventricular volume. Blood is more conductive than most tissues,
and the increased volume of the heart during diastole\index{diastole}  than systole,
means the 
 electrical current ``prefers'' to propagate through the blood\index{blood}.
Next, we 
illustrate the process of EIT data collection. Current is applied between
a pair of electrodes\index{electrode} and then propagates diffusely throughout the body, creating  
distribution of voltage shown by the equipotential lines. Between systole and
diastole\index{diastole}, the changing blood\index{blood} volume in the heart affects the electric propagation
thus the voltage measured at the electrodes\index{electrode}.
\Figref{fig:ch01:EIToverview2} illustrates EIT image reconstruction\index{reconstruction}
and calculation of functional measures. Based on the data measured, a reference
data set (frame) is chosen and images are reconstructed of the current data frame
with respect to the reference, using a computational model to project and
then filter\index{filter} the image. Reconstructed images of a transverse slice are 
calculated for each frame as a function of time. From these parameters,
pixel\index{pixel} waveforms\index{waveform} can be extracted (showing activity in the heart and lungs)
measures (such as the heart rate or tidal\index{tidal} volume in each lung\index{lung}) calculated.

\subsection{Tissue electrical properties}

The electrical properties of biological tissue are measured
by applying and measuring voltages and currents. EIT applies
imperceptible currents which meet the safety standards for 
medical devices (ISO/IEC 60601).
 In order to improve electrical 
safety and to remain outside the range of the body's own
electrical signals, EIT normally applies sinusoidal\index{sinusoidal} currents
at frequencies above 50\,kHz where the standards
allow a maximum current of 5\,mA 
(\secref{sec:ch02:ElectricalSafetyandCurrentLimitations}).

At these small currents,
Ohm's law applies: $V=IR$,
a voltage $V$ is required to send a
current $I$ through a
resistance $R$. An analogy with
fluid flow is useful: in order to force flow (current) through
a narrowed pipe (resistance) a pressure (voltage) is required.
Resistance (in \si{\ohm}) describes the global properties of a body
between electrodes\index{electrode}. 
To describe the electrical properties of tissue, the relevant
parameter is the resistivity $\rho$ (normally measured in \si{\ohm}$\cdot$cm).
For a uniform cylinder with electrodes\index{electrode} at the ends,
 the resistance, $R=l\rho/A$, increases with length
with the length, $l$,  and decreases with area, $A$. 
It's often useful to think in terms of the inverse of resistance
$G=R^{-1}$ measured in Siemens (S) and
conductivity $\sigma = \rho^{-1}$ in units of S/cm or S/m.
In the body, electric current travels as ions in solution, and blood\index{blood} is one
of the best conductors.

When our sinusoidal\index{sinusoidal} current $I = I_0 \cos 2\pi f$ is applied to
a pure resistor, $V$ is exactly in phase with $I$. In contrast\index{contrast}, when 
a sinusoidal\index{sinusoidal} current flows into capacitor $C$, voltage is delayed by
90\si{\degree}. 
When an electrical circuit\index{circuit} combines resistances and capacitances\index{capacitance},
it is convenient to use complex\index{complex} numbers to express the in- and out of phase
components at a single sinusoidal\index{sinusoidal} frequency. With complex\index{complex} values,
impedance\index{impedance} $Z$ corresponds to resistance and 
admittance\index{admittance} $Y$ corresponds to conductance\index{conductance}.
For purely resistive circuits\index{circuit},
$Z=R$ and $Y=G$. When a capacitor is in parallel to a resistor, the
complex\index{complex} admittance\index{admittance} $Y= G + \im 2\pi f C$.

In biological tissue at low frequency, tissue membranes\index{membrane} act as 
capacitors which store electrical energy. As the frequency increases,
a pathway through the membrane\index{membrane} has a higher admittance\index{admittance}, and current
will begin to flow through the membrane\index{membrane} (\figref{fig:ch02:f12}).
The bulk properties of tissue are characterized by a complex\index{complex}
conductivity (or admittivity\index{admittivity}) with a symbol $\sigma^*$ (or $\gamma$). As the
frequency increases, the change in $\sigma^*$ can be used
to characterize the tissue (\figref{fig:ch02:f03}).
EIT systems typically measure both the amplitude and phase of the voltage
at the electrodes\index{electrode}, but most image reconstruction\index{reconstruction} algorithms use
only the amplitude. Thus conductivity and admittivity\index{admittivity} are treated
as near-synonyms in this much of this book.

In EIT, a current at frequency $f$ is applied to the body part of
interest across
a pair of electrodes\index{electrode}. The current spreads out though the
body to find the easiest (lowest impedance\index{impedance}) path
(\figref{fig:ch01:overview}C). Changes in the spatial distribution
of $\sigma^*$ of tissue will change the way the current flows
and change the pattern of voltage.
EIT is sensitive to contrasts\index{contrast} in tissue admittivity\index{admittivity}.
Since blood\index{blood} is more conductive than most other tissue, an increase
of blood\index{blood} in the heart during diastole\index{diastole}, or pooling of blood\index{blood}
due to hemorrhage\index{hemorrhage}, create changes that can be measured by EIT.
The most important conductivity contrast\index{conductivity contrast} in the body is air. A 
large volume of non-conductive air moves with each breath, and,
as a result, EIT imaging of breathing is easiest.
Several other tissues give useful constrasts\index{contrast}. Neural tissue
changes conductivity when in an active state (chapter \ref{ch11:main}).
Cancerous tissue and the new growth around tumours contrast
with benign tissue (chapter \ref{ch12:main}). Additionally, conductivity
contrasts\index{contrast} can be induced using hypertonic\index{hypertonic} saline injections
(chapter \ref{ch10:main}), ingesting salty meals, or through
thermal contrasts\index{contrast} (chapter \ref{ch13:main}).

\subsection{EIT Electronics}

Most EIT systems apply currents across pairs of electrodes\index{electrode} and measure
the voltages across the remaining electrodes\index{electrode}. This arrangement has
an advantage in compensating for variable electrode contact quality.
It is also typical to not use measurements made on the driven\index{driven} electrodes\index{electrode}
for the same reason.
A \emph{frame} of EIT data refers to a complete set of measurements. Most EIT
systems
apply, in sequence, current across each pair of electrodes\index{electrode} and then
measure voltage on all other electrode pairs, before switching the
current to the next pair.
Early systems treated adjacent electrodes\index{electrode} as pairs, but many modern
systems will use a \emph{skip} pattern, where skip=2 means that electrode 1
is paired with electrode 4. The choice of current injection and
voltages measurements is called the 
\emph{simulation and measurement procotol}.
Given $N$ electrodes\index{electrode},
 there are a possible $N\times(N-3)/2$ independent measurements.
Typical systems have 8, 16, or 32 electrodes\index{electrode}, and thus have 
20, % 8/2 x 5 = 20 
104, or % 16/2 x 13 = 104 
464 % 32/2 x 29 = 464
measurements per frame.
Modern EIT systems function at impressive speeds, measuring 50 or
more frames per second. For a 32 electrode system with a current
injection at 100\,kHz, this corresponds to 62.5 sinusoidal\index{sinusoidal} periods
per pattern. An EIT system must calculate the amplitude and 
phase on all electrodes\index{electrode} during this time.

There are numerous challenges to obtaining high quality EIT data.
As mentioned, the high frame rates mean that it is difficult
to perform lots of signal averaging. The hospital environment
is electrically noisy and a patient generates their
own electrical signals (heart, muscles and nerves).
Since EIT signals are in a narrow
range of frequencies, precision filters\index{filter} are required to 
reject this interference. An additional challenge is the precision
required, as seen in \figref{fig:ch01:EIToverview1} where large
changes of blood\index{blood} in the heart lead to small changes in
the voltage equipotential lines on the body surface. A good
EIT system should be able to accurately measure changes of 0.1\%.
Several further challenges arise. The dynamic range (ratio between
the largest and smallest signals) is large, requiring
precise data acquisition. At audio frequencies, circuits\index{circuit} have
numerous defects, non-zero common-mode rejection, leakage
currents, and crosstalk between electrode channels. 
Precision electronics are sensitive to temperature\index{temperature} and thus
can drift over time. Finally, the contact quality of electrodes\index{electrode}
is variable: electrode contact often improves as a subject
sweats, but electrodes\index{electrode} can disconnect as posture changes.
These challenges are difficult, but most modern EIT systems
work surprisingly well most of the time.

For routine use, EIT electronics must be robust and user
friendly. Disconnected electrodes\index{electrode} must be detected and reported.
Application of individual ECG-type\index{ECG} electrodes\index{electrode} is adequate for research,
but too slow for clinical use. Instead, manufacturers have designed
electrode belts which can be rapidly placed. Belts need to be of
biocompatible material and either washable or cheap enough for
one-time use. To improve contact quality a contact gel or saline
water is normally applied to the skin. This leads to a further
challenge that the saline/sweat mix is very corrosive and
damages belts.

Time-difference EIT images typically use one measurement frequency.
An alternative
approach is to compare the difference between impedance\index{impedance} images measured
at different measurement frequencies, termed 
frequency-difference EIT\index{frequency-difference EIT|seealso{fdEIT}} (fdEIT\index{fdEIT}).
This technique exploits the different impedance\index{impedance} characteristics of tissues
at different measurement frequencies. An example of such a contrast\index{contrast} would
be the difference between
cerebro-spinal fluid (CSF\index{CSF})
 and the grey matter of
the brain\index{brain}. As the CSF\index{CSF} is an acellular, ionic solution, it can be considered a
pure resistance, so that its impedance\index{impedance} is identical and equal to the resistance
for all frequencies of applied current. However, the grey matter, which has
a cellular structure, has a higher impedance\index{impedance} at low frequencies than at
high frequencies. This frequency difference could be exploited
to provide a contrast\index{contrast} in the impedance\index{impedance} images obtained at different
frequencies, and identifying different tissues in a
multifrequency EIT image \cite{Yerworth2003Electrical}.
\begin{figure}[!ht]\centering
\includegraphics[width=0.7\columnwidth]{\CURRCHAP/fig-EIT-example2/EIT-overview.pdf}
\FIGCAPTION{fig:ch01:EIToverview2}{\CURRCHAP/fig-EIT-example2}
\end{figure}

\subsection{Models of Sensitivity}


Sensitivity is the change in a measurement due to a change in an internal parameter.
It is characterized by a Jacobian\index{Jacobian} matrix,
 $\JB_{i,j} = \partial \vB_i/\partial \sigma^*_j$,
which indicates how voxel\index{voxel} $j$ in the body affects measurement $i$.
In order to reconstruct an image, the sensitivity\index{sensitivity} explains what each
measurement ``means'' and which parts of the body could be generating
the measurements observed (chapter \ref{ch04:main}). At the frequencies
used in EIT, voltages and currents distribute according to the Laplace equation\index{Laplace}:
$\nabla \cdot \sigma^* \nabla V = 0$,  with boundary conditions of
the applied voltages. The Laplace equation means that EIT is non-local;
every change in $\sigma^*$ anywhere in the body affects voltages everywhere,
and leads to the ill-conditioning\index{ill-conditioning} of image reconstruction\index{reconstruction}.

In EIT, two main approaches to sensitivity\index{sensitivity} calculation have been used, analytical
and
finite element models\index{finite element models|seealso{FEM}} (FEM\index{FEM}).
Analytical models are useful for regular shapes
such as cylinders, and help understand the theoretical limits of sensitivity\index{sensitivity}.
For patient application, the FEM\index{FEM} has been the main tool. An FEM splits a body
into subdomains (normally tetrahedra) and solves the Laplace equation\index{Laplace equation} by
constraints between the subdomain boundaries. Increased accuracy is achieved
by a finer subdivison of the domain, especially in the high electric field
regions near electrodes\index{electrode}. The method is fairly rapid: a modern computer solves
a one million element FEM\index{FEM} in a few seconds.

Sensitivity matrix calculations allow an understanding of EIT system 
configurations. For example, where are electrodes\index{electrode} best placed to 
achieve the best detectability\index{detectability} of a given process?
\cite{Gilad2009Modelling, Graham2006Objective, Grychtol2019Thoracic}
An important question concerns the accuracy required of FEMs\index{FEM}. Most 
algorithms use a \emph{atlas} model based on an approximate shape
(cylinder for early algorithms \cite{Barber1984Applied}, and CT-based
FEM\index{FEM} models for more recent ones). The quality of images certainly
improves with accurate models (see \figref{fig:ch05:GREIT-human}), but
these are rarely available. Even given patient-specific CT, the electrode
positions would not be perfect, and posture changes \cite{Coulombe2005Parametric}
and breathing \cite{Adler1996Impedance} would make changes to the shape.
FEM\index{FEM} meshing variability can be corrected through setting of image
reconstruction\index{reconstruction} parameters \cite{Adler2011Minimizing}.

\subsection{EIT Image Reconstruction}

\emph{Image reconstruction\index{reconstruction}} is the term used in tomographic
imaging for calculation of an image from projection
data. Image reconstruction is challenging as the underlying equations
are ill-conditioned\index{ill-conditioning} and often ill-posed\index{ill-posed}.
The ill-conditioning\index{ill-conditioning} stems from the
large difference in sensitivity\index{sensitivity} between regions
(the electrodes\index{electrode} and the
body centre in EIT). EIT is typically ill-posed\index{ill-posed} because it is not possible
to estimate a large number of image parameters from the  limited number of
measurements in each data frame.
Image reconstruction\index{reconstruction} calculates
an estimate, $\xH$, of
the distribution of internal properties, $\xB$, which best explains
the measurements, $\yB$.
A simplified schema for image reconstruction\index{reconstruction} is shown in 
\figref{fig:ch05:overview},
which illustrates the process by which model parameters are iteratively\index{iterative} adjusted to
fit the measurements.

Image reconstruction\index{reconstruction} in EIT has been divided into algorithms
for difference EIT (\secref{sec:ch05:DifferenceEIT})
and absolute EIT (\secref{sec:ch05:AbsoluteEIT}). Difference EIT is more stable and
can be implemented as a matrix multiplication.
Absolute EIT\index{absolute EIT|seealso{aEIT}} is a more difficult problem, and requires
accurate models of
 body geometry and electrode positions, shapes and contact impedances.
For difference EIT, shape and electronics modelling inaccuracies are less significant,
as long as they remain the same between the difference measurements.
While in geophysical ERT\index{ERT}, absolute reconstruction\index{reconstruction} is
common, experimental and clinical EIT has almost exclusively used difference
measurements and algorithms.
Image reconstruction\index{reconstruction} in EIT is a difficult problem and is highly sensitive
to noise and outliers; \secref{sec:ch05:WhyisEITsohard} addresses the 
question ``Why is EIT so hard?''.

EIT image reconstruction\index{reconstruction} is formulated to minimize a norm
$
\| \yB - F(\xH) \|^2_\WB + \alpha^2\| \LB( \xH - \xB_0 ) \|^2,
$
where the first term, $\yB - F(\xH)$, is the ``data mismatch''
between the measured data and their estimate via the forward model, $F(\cdot)$.
$\WB$ is a data weighting matrix, and represents the inverse
covariance\index{covariance} of measurements.
The second term is penalty term on the mismatch between the reconstruction\index{reconstruction}
estimate, $\xH$, and an {\em a priori\index{a priori}} estimate of its value, $\xB_0$.
The matrix $\LB$ is normally chosen to measure the amplitude or 
roughness in the image. Thus this term penalizes solutions which
would match the data but are large or non-smooth\index{non-smooth}.
The relative weighting between the data and prior mismatch terms
is controlled by a hyperparameter\index{hyperparameter}, $\alpha$. When $\alpha$ is
large, solutions tend to be smooth and more similar to the prior;
for small $\alpha$, solutions better match the data, 
have higher spatial resolution,
but are noisier and less well conditioned (\figref{fig:ch05:hyperparameter}).

Early work in EIT image reconstruction\index{reconstruction} was motivated by the backprojection
algorithms of X-ray\index{X-ray} CT. Interestingly, it is possible to interpret
regularized\index{regularization} linear inverse in terms of backprojection and filtering
(see Box \ref{box:ch05:TikhonovBackprojection}). A regularized\index{regularization}
inverse algorithm has many user-selected parameters, which control
its performance. A standardization\index{standardization} effort in 2007 lead to the 
GREIT\index{GREIT} algorithm \cite{Adler2009GREIT}, based on an consensus
of the figures of merit required. Interestingly, high resolution was
not a priority: clinical and experimental users want robust and
predictable images.

Current work in algorithms is pursuing two directions. One seeks
to increase the robustness of images given
electrode movement \cite{Soleimani2006Imaging}
and detecting
and compensating for failing electrodes\index{electrode} \cite{Asfaw2005Automatic}.
The other pathway seeks novel techniques to improve image
reconstruction\index{reconstruction}, using innovative methods such as
 direct methods (chapter \ref{ch06:main}),  
level sets \cite{Rahmati2012Level}, and
machine learning \cite{Rymarczyk2018Implementation}.


\subsection{Image Interpretation}

Medical images are valuable only if they can provide
diagnostic information to usefully inform 
treatment.
EIT images can thus be analyzed to 
determine relevant parameters:
a rich literature of
\emph{functional EIT\index{functional EIT|seealso{fEIT}}}
(or fEIT\index{fEIT}) parameters
has developed for analysis of individual images and time-sequences of images
(chapter \ref{ch07:main}).
Techniques for fEIT\index{fEIT} analysis were subdivided
 into \emph{functional EIT images} and
\emph{EIT measures}
\cite{Frerichs2017Chest}.
 In \figref{fig:ch01:EIToverview2} the
parameters calculated are the heart rate (HR) and an index
of relative ventilation level between the right and left lungs.
fEIT\index{fEIT} image analysis is illustrated in 
\figref{fig:ch07:fEIT}. Two basic categorizations are
between measures which can be measured continuously, and
examination-specific measures, which require specific interventions
with patients.
The literature on fEIT\index{fEIT} measures includes:
\begin{itemize}[nosep]
\item {\em Distribution of lung\index{lung} volumes and flows},
relevant for managing ventilated patients and
monitoring obstructive lung\index{lung} disease \cite{Vogt2018Regional}.
\item {\em Distribution of Aeration\index{aeration} change}, 
due to an intervention
or physiological activity.
\item {\em Frequency analysis of impedance\index{impedance} changes},
to separate breathing- and heart-related effects, using
an ECG-gated\index{ECG} or frequency-filtered\index{frequency-filter} signal,
leading to the \emph{perfusion-related\index{perfusion-related}} EIT signal
\cite{Adler2017Origin, Ferrario2012Toward}.

\item {\em Respiratory system mechanics}:
Lung tissue can be characterized by 
a compliance\index{compliance}, $C$
and resistance, $R$.
$C$ 
is low at both low and high lung\index{lung} volumes due
to alveolar\index{alveolar} collapse (atelectasis\index{atelectasis}) or overdistension
\cite{Costa2009Bedside}.
The time constant $\tau = RC$ of tissue introduces
a ventilation which varies across the lung\index{lung}
functional EIT measures of time constant \cite{Miedema2012Regional}.

\item {\em Activation patterns}:
Functional EIT of brain\index{brain} EIT images can
characterize 
the pattern of activation within the
cortex following stimulation
\cite{Aristovich2014Method}.

\item {\em Pulse transit time}, of
blood\index{blood} through the vasculature
is modulated by the blood pressure\index{blood pressure}, and can measure
pulmonary\index{pulmonary}-
\cite{Proenca2016NoninvasivePulmonary} (transit time from
heart to lungs) and
systemic-arterial\index{artery} pressure (transit time from
heart to aorta\index{aorta})
\cite{Sola2011Noninvasive}.

\item {\em Contrast agents}:
Direct measures of blood flow\index{blood flow} can be measured
with injected conductivity contrasts\index{conductivity contrast}. 
EIT imagess show
the blood flow\index{blood flow} through the
heart and lungs\cite{Adler2020Detection, Frerichs2002Regional} or
brain\index{brain}\cite{Adler2017Cerebral}.

\end{itemize}

\section{EIT Applications and Perspectives}


In the second part of this book, we review applications of EIT 
in many fields,
from imaging lungs, heart and blood flow\index{blood flow}, brain\index{brain} and nerves,
cancerous\index{cancerous} tissue and other applications.
Good reviews of EIT applications have
been written, to which we refer the interested reader 
\cite{Adler2012Whither,
      Adler2017Electrical, 
      Bodenstein2009Principles,
      Costa2009Electrical,
      Frerichs2017Chest,
      Frerichs2014Electrical,
      Leonhardt2012Electrical,
      Lundin2012Electrical,
      Nguyen2012Review}.
Some applications are starting to become established in routine
clinical use, such as monitoring
of ventilation (chapter \ref{ch09:main}), while others are still at
an earlier stage.
Overall EIT has several clear advantages: it is non-invasive\index{non-invasive} and
minimally cumbersome, requiring only electrodes\index{electrode} and wires on the body;
the electronics hardware is potentially fairly cheap; it is suitable
for prolonged monitoring. At the same time, EIT images are of
low resolution, EIT data are subject to many sources of interference,
and we still don't know how to do absolute image reconstruction\index{reconstruction} reliably.

The concepts in medical EIT are also used in
geophysics and process tomography\index{tomography}.
Geophysical imaging with electrical measurements has a
long history \cite{Allaud1977Schlumberger}; it can measure
the metallic ores and ground water, and is used
for imaging (e.g.\ archaeological surveys) and 
monitoring (bridges and embankments).
Process tomography\index{tomography} applications of ERT\index{ERT} focus on monitoring
of pipes and mixing vessels.

It is useful to review the two decades since the publication of the
first edition of this book \cite{Holder2004Electrical}, perhaps in the
context of a comment from our editorial \cite{Adler2015EIT}
\begin{quote}
We are looking forward to
the day when there is no longer a need to explain EIT; when there is no longer
the need to call it a promising, ``new'' technology. Instead, we would like to
write, ``Based on evidence of improved patient outcomes and safety,
EIT devices are increasingly used \ldots'', and provide the
references to prove it.
\end{quote}

In the last decade, EIT has begun to see early clinical use.
There are now several companies which sell clinical chest EIT systems.
The
COVID-19\index{COVID-19} pandemic has underlined the importance of intensive care
medicine and the management of ventilated patients, a role for
which EIT's advantages are clear \cite{vanderZee2020Electrical}.
There
is a strong and growing rate of citations of EIT in the medical
literature. On the other hand,
non-lung\index{lung} applications of EIT have not yet
seen a transition to clinical use, but many are showing clear promise,
such as monitoring of nerves \cite{Ravagli2020Imaging}.

To our readers:
if you've read this far, you are interested in better understanding
EIT, perhaps to conduct novel research or better treat patients. We eagerly
await learning about your insights. We encourage you to interact with the
EIT community, via our conferences (\secref{sec:ch19:EITConferences})
 software and mailing
lists \cite{Adler2019EIDORS}. Our community is encouraging
and supportive.



\ifdefined\BUILDCHAPTERALONE
 \bibliography{../bib/book} \bibliographystyle{ieeetr}
 \end{document}
\fi
