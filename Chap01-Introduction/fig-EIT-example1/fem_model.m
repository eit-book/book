function img = fem_model(hcut, topcut);
    if nargin==0; hcut = 2.0; end
    if length(hcut)==1; hcut(2:3) = [0.15,0.45]; end
    if nargin<=1; topcut = 0.0; end
    hcut0 = hcut(1);
    extra={'lungs', 'heart', 'arms', sprintf([ ...
       'solid mainobj_bot= plane(0,0,%f;0,0,-1) -maxh=0.02;' ...
       'solid top= plane(0,0,%f;0,0,-1) -maxh=0.02;' ...
       'solid llung = ellipsoid(-0.40,-0.05,1.5;0,0,1.2;0,0.6,0;0.55,0,0);' ...
       'solid rlung = ellipsoid( 0.40,-0.05,1.5;0,0,1.2;0,0.6,0;0.55,0,0);' ...
       'solid lbot = plane(0,0,%f;0,0,1) and top;'  ...
       'solid heart_= sphere(0.1, %f,1.8;%f) -maxh=0.05;' ...
       'solid heart= heart_ and lbot;' ...
       'solid lungs= (llung or rlung) and lbot and not heart_;' ...
       'solid lcut= plane(-1,0,2;-3,0, 1);' ...
       'solid larm= cylinder(0,0,-2;-1,0,0;0.4);' ...
       'solid larm_= larm and top and lcut;' ...
       'solid rcut= plane( 1,0,2; 3,0, 1);' ...
       'solid rarm= cylinder(0,0,-1; 1,0,0;0.4);' ...
       'solid rarm_= rarm and top and rcut;' ...
       'solid arms= larm_ or rarm_;' ...
           ], topcut*[1,1], hcut)};
    fmdl= ng_mk_ellip_models([hcut0,1.2,0.8], ...
              [16,1.5],[0.2,0.4,0.06],extra); 
    fmdl.nodes(:,3) = -fmdl.nodes(:,3);
    img= mk_image(fmdl, 1);
    img.elem_data(fmdl.mat_idx{2}) = 0.6;
    img.elem_data(fmdl.mat_idx{3}) = 1.5;

