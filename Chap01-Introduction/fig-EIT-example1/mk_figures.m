function mk_figures(fignos)


  for fig = fignos(:)'
     switch fig; 
        case 1; mk_figure1
        case 2; mk_figure2
        case 3; mk_figure3
        case 4; mk_figure4
        case 5; mk_figure5
        otherwise; error('huh?')
     end
  end

function mk_figure1
    img = fem_model(2.1);
    mi = img.fwd_model.mat_idx;
    img.elem_data(mi{2}) = 0.55;
    img.elem_data(mi{3}) = 1.5;
    opt.viewpoint = struct('az',1,'el',-35);
    opt.edge.width = 0;
    opt.edge.significant.color = [139,69,19]/256;
    img.calc_colours.ref_level= 0.95;
    img.calc_colours.clim= 1;
     show_fem_enhanced(img,opt);
    axis off
    print_convert baby_fem.png

function mk_figure2
    for fn = 1:2; switch fn
        case 1; img = fem_model([2.1,0.15,0.45],0.8);
        case 2; img = fem_model([2.1,0.2 ,0.40],0.8);
        end    
        figure(fn)

        mi = img.fwd_model.mat_idx;
        img.elem_data(mi{2}) = 0.55;
        img.elem_data(mi{3}) = 1.5;
        opt.viewpoint = struct('az',1,'el',-35);
        opt.edge.width = 0;
        opt.edge.significant.color = [139,69,19]/256;
        img.calc_colours.ref_level= 0.95;
        img.calc_colours.clim= 1;
        show_fem_enhanced(img,opt);
        axis off
        print_convert(sprintf('baby_heart%d.png',fn))
    end

function mk_figure3
    fig_with_equipotential(1.8,[0.15,0.45],[1,6,1,6])
    print_convert equip1_diastole.png
    fig_with_equipotential(1.8,[0.2 ,0.40],[1,6,1,6])
    print_convert equip1_systole.png
    fig_with_equipotential(1.8,[0.15,0.45],[16,5,1,6])
    print_convert equip2_diastole.png
    fig_with_equipotential(1.8,[0.2 ,0.40],[16,5,1,6])
    print_convert equip2_systole.png

function [img,opt] = basic_fem(zlevel,heart);
    img = fem_model([zlevel,heart]);
    img.fwd_model.nodes(:,3) = img.fwd_model.nodes(:,3) + zlevel;
    mi = img.fwd_model.mat_idx;
    img.elem_data(mi{2}) = 0.50;
    img.elem_data(mi{3}) = 1.5;
    opt.viewpoint = struct('az',1,'el',-75);
    opt.edge.width = 0;
    opt.edge.significant.color = [139,69,19]/256;

    img.calc_colours.ref_level= 0.95;
    img.calc_colours.clim= 2;
    if nargout > 0; return ; end
    show_fem_enhanced(img,opt);
    cmap = colormap;
    cmap = cmap(1:end-3,:);
    cmap(140:end,1) = cmap(140:end,1:2)*[1;1]/1.5;
    cmap(140:end,2) = cmap(140:end,2)/3;
    img.calc_colours.cmap_type= cmap;
    show_fem_enhanced(img,opt);

function fig_with_equipotential(zlevel, heart, ssmm)
    clf
    imgs = equipotential_slice(zlevel,heart, ssmm);
    show_slices(imgs);
    hh= get(gca,'Children');
    delete(hh(2))
    hold on
    basic_fem(zlevel,heart);
    hold off
    axis off

function img = equipotential_slice(zlevel,heart,ssmm)
    img = fem_model([zlevel+0.1,heart]);
    mi = img.fwd_model.mat_idx;
    img.elem_data(mi{2}) = 0.3;
    img.elem_data(mi{3}) = 2.0;
    img.fwd_model.stimulation = stim_meas_list(ssmm, num_elecs(img));
    img.fwd_solve.get_all_nodes = true;
    vv= fwd_solve(img);
    img = rmfield(img,'elem_data');
    img.node_data = vv.volt;
    img = img_slice(img, zlevel);

function img = img_slice(img, zlevel)
    msm = struct('level', [inf,inf,-zlevel], ...
           'x_pts', linspace(-1.2,1.2,101), ...
           'y_pts', linspace(-0.8,0.8,101));

    img.show_slices.axes_msm= true;
    img.fwd_model.mdl_slice_mapper = msm;
    img.show_slices.contour_levels = 20;
    img.show_slices.contour_properties = {'Color',[0,0,0],'LineWidth',2};
    img.calc_colours.backgnd= [1,1,1];

function fig_with_sensitivity(zlevel, heart, ssmm)
    clf
    imgs = sensitivty_slice(zlevel,heart, ssmm);
    show_slices(imgs);
    hh= get(gca,'Children');
    delete(hh(2))
    hold on
    basic_fem(zlevel,heart);
    hold off
    axis off


function img = sensitivty_slice(zlevel,heart,ssmm)
    img = fem_model([zlevel+0.1,heart]);
    mi = img.fwd_model.mat_idx;
    img.elem_data(mi{2}) = 0.3;
    img.elem_data(mi{3}) = 2.0;
    img.fwd_model.stimulation = stim_meas_list(ssmm, num_elecs(img));
    J= calc_jacobian(img);
    J = J'./get_elem_volume(img);
    img.elem_data = J.*img.elem_data;
    filt = ones(5); filt = filt/sum(filt(:));
    img.calc_slices.filter = conv2(filt,filt);
    img = img_slice(img, zlevel);

function fig_with_image(zlevel, heart)
    clf
    [img,opt] = basic_fem(zlevel,heart);
    opt.viewpoint.el = -70;
    th = 35*pi/180;
    rot = [cos(th), sin(th); -sin(th), cos(th)];
    img.fwd_model.nodes(:,[1,3]) = ...
    img.fwd_model.nodes(:,[1,3])*rot;
    colormap(gray(256));
    img.elem_data(img.fwd_model.mat_idx{3}) = 1;
    show_fem_enhanced(img,opt);
    cmap = colormap;
    axis off
    imgs = reconst_images;
    imgs.fwd_model.electrode(:) = [];
    imgs.fwd_model.nodes(:,1) = -imgs.fwd_model.nodes(:,1);
    imgs.fwd_model.nodes(:,[1,3]) = ...
    (imgs.fwd_model.nodes(:,1)*[1,0])*rot - [0,0.01];

    th = linspace(0,2*pi,51);
    imgc = imgs;
    hold on
    for i = 0:5;
        xofs = i*0.8;
        imgc.elem_data = imgs.elem_data(:,66+i);
        imgc.fwd_model.nodes(:,1) = ...
        imgs.fwd_model.nodes(:,1) + xofs;
        set(show_fem(imgc),'EdgeAlpha',0);
        K=1.02;
        plot3(K*1.2*rot(1,1)*cos(th) + xofs + .02, ...
              K*0.8*sin(th), ...
              K*1.2*rot(1,2)*cos(th),'k', 'LineWidth',2)
    end
    hold off
    colormap(cmap)
    print_convert image_sequence.png
%figure(2);show_slices(imgs);

function imgs = reconst_images
    img = fem_model(3.0);
    fmdl = mdl_normalize(img.fwd_model,1);
    [fmdl.stimulation,fmdl.meas_select] = mk_stim_patterns(16,1,'{ad}','{ad}');
    img.fwd_model = fmdl;
    img.elem_data(fmdl.mat_idx{2}) = 0.4;
    img.elem_data(fmdl.mat_idx{3}) = 1.5;

    opt.imgsz = [64 64]; opt.noise_figure = 0.5;
    imdl = mk_GREIT_model(mk_image(fmdl,1), 0.25, [], opt);
    vv= eidors_readdata('P04P-1016.get'); vi=vv; vh=vv(:,61);
    imgs = inv_solve(imdl,vh,vi);
    imgs.calc_colours.ref_level = 0;
    imgs.calc_colours.clim = 15;
    imgs.calc_colours.transparency_thresh =0;

    
function mk_figure4
    fig_with_sensitivity(1.8,[0.15,0.45],[1,6,8,10])
    print_convert equipotential.png

function mk_figure5
    fig_with_image(1.6,[0.15,0.45])
%   print_convert equipotential.png
     
