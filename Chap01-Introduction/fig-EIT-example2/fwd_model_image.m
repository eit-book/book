if 0
fmdl = mk_library_model({'adult_male', ...
    'boundary'}, ...
            [16 1 0.5],[0.05,0.02],1);
end
fmdl = mk_library_model({'neonate','boundary','left_lung','right_lung'},[16 1 0.5],[0.1 0.1 -1 0 60],1,49);
img = mk_image(fmdl, 1); % background conductivity
img.elem_data(fmdl.mat_idx{2}) = 0.3;
img.elem_data(fmdl.mat_idx{3}) = 0.3;
clf;
opt.viewpoint = struct('az',0,'el',14);
opt.edge.width = 0;
show_fem_enhanced(img,opt);
axis off
print_convert fwd_model_image.jpg

