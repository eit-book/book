function img3 = img3_reconst;
    skip4 = {32,1,[0,5],[0,5],{'no_meas_current_next1'},1};
    [stim, msel] = mk_stim_patterns(skip4{:});
    idx = elec_rearrange( [16,2], 'square');
    pp = struct('zCUT', 1.5,'stim',stim,'msel',msel);
    pp.doarms = false;
    pp.z_lungs = 0.10;
    img = fem_model_arms( pp);
    img.fwd_model.electrode(idx) = img.fwd_model.electrode;

    vh = fwd_solve(img);
    pp.z_blood = 1.2;
    pp.z_lungs = 0.09;
    img = fem_model_arms( pp);
    img.fwd_model.electrode(idx) = img.fwd_model.electrode;
    vi = fwd_solve(img);

    vopt.imgsz = [20,20];
    vopt.square_pixels = true;
    vopt.zvec = linspace(0.05,0.40,11);
    vopt.save_memory = 1;
    opt.noise_figure = 4.0;

    % GREIT 3D with 2x16 electrode belt
    [imdl,opt.distr] = GREIT3D_distribution(img.fwd_model, vopt);
    imdl3= mk_GREIT_model(imdl, 0.20, [], opt);
    img3 = inv_solve(imdl3, vh,vi);
    img3.calc_colours.ref_level = 0;


