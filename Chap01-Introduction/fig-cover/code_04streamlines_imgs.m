clf
opt= struct();
opt.edge.width=0;
opt.viewpoint = struct('az',10,'el',13);
opt.edge.significant.viewpoint_dependent.callback = false;

pp = struct('zCUT',-0.1);
pp.doarms = true;
img = fem_model_arms( pp) ;
img.calc_colours.clim= 0.5;
imgc = crop_model(img,@(x,y,z) y<-.085 & x>-.0499);
imgc.elem_data(:) = imgc.calc_colours.ref_level;
show_fem_enhanced(imgc,opt)

pp.zCUT = 1.5; pp.SSMM = [5,26,5,26]; 
pp.contours = 10*linspace(-1,1,19);
img_contours(pp)

img3 = img3_reconst;
xyz = img3.fwd_model.coarse2fine'* ...
          interp_mesh(img3);
cut = (xyz(:,1) > -0.35) & (xyz(:,2)< -0.30);
img3.elem_data( cut ) = 0;
cmap = colormap;
hold on
hh=show_3d_slices(img3,[0.18,0.235,0.29],[-0.10],[]);
delete(horzcat(hh{:}))
hold off
colormap(cmap); axis off

print_convert(sprintf('%s-f1.jpg',mfilename));
