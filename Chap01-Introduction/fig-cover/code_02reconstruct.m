skip4 = {32,1,[0,5],[0,5],{'no_meas_current_next1'},1};
[stim, msel] = mk_stim_patterns(skip4{:});
pp = struct('zCUT', 1.5,'stim',stim,'msel',msel);
pp.doarms = false;
img = fem_model_arms( pp);
idx = elec_rearrange( [16,2], 'square');
img.fwd_model.electrode(idx) = img.fwd_model.electrode;
show_fem(img, [0,1.016]); view(0,0)

pp.z_lungs = 0.10;
img = fem_model_arms( pp);
vh = fwd_solve(img);
pp.z_blood = 1.2;
pp.z_lungs = 0.09;
img = fem_model_arms( pp);
vi = fwd_solve(img);

clear opt
vopt.imgsz = [64,64];
vopt.square_pixels = true;
vopt.zvec = linspace(0.18,0.32,5);
vopt.save_memory = 1;
opt.noise_figure = 4.0;

% GREIT 3D with 2x16 electrode belt
[imdl,opt.distr] = GREIT3D_distribution(img.fwd_model, vopt);
imdl3= mk_GREIT_model(imdl, 0.20, [], opt);

%show_3d_slices(img,[1.6,2.0,2.4],[],[0.4]);
imgr = inv_solve(imdl3, vh,vi);
imgr.calc_colours.ref_level = 0;
%show_3d_slices(imgr);%,[1.6,2.0,2.4],[],[0.4]);
show_3d_slices(imgr,[0.20,0.28],[-0.12],[]);
view(10,13); axis equal; axis off

print_convert(sprintf('%s-f1.jpg',mfilename));


