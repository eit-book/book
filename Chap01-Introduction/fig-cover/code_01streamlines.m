clf
opt= struct();
opt.edge.width=0;
opt.viewpoint = struct('az',10,'el',13);
opt.edge.significant.viewpoint_dependent.callback = false;

SSMM = [5,26,5,26];

cm=-8.5;
pp = struct('zCUT',-0.1);
pp.doarms = true;
img = fem_model_arms( pp) ;
img.fwd_model.nodes = img.fwd_model.nodes *100; % cm
img.calc_colours.clim= 0.5;
imgc = crop_model(img,@(x,y,z) y<cm & x>-4.99);
show_fem_enhanced(imgc,opt)

pp.zCUT = 1.5;
img = fem_model_arms( pp) ;
img.fwd_model.nodes = img.fwd_model.nodes *100; % cm
x_pts = linspace(-5,15,301); y_pts = linspace( 0,45,201);
msm = struct('level',[inf,-5,inf], 'x_pts', x_pts, 'y_pts', y_pts);

img.fwd_model.stimulation = stim_meas_list(SSMM,num_elecs(img));
img.fwd_solve.get_all_nodes = true;
vh = fwd_solve(img);
imgr = rmfield(img,'elem_data');
imgr.node_data = vh.volt;
imgr.fwd_model.mdl_slice_mapper = msm;
Filt = ones(3)/9; Filt = conv2(Filt,Filt);
%imgr.calc_slices.filter = Filt;
imgr = calc_slices(imgr);

contours = .08*linspace(-1,1,19);

hold on
clear FakeVol; for fi=1:2; % Horrible to fight matlab 
    FakeVol(fi,:,:) = permute(imgr,[3,2,1]);
end
hh=contourslice(x_pts, [-15,15],y_pts, FakeVol, ...
     [],-5.1,[], contours);
for hi = hh;
    set(hh,'EdgeColor', [.2,.3,.2],'LineWidth',2);
end
hold off; axis off
print_convert(sprintf('%s-f1.jpg',mfilename));
