function img_contours(pp)
    pp.zCUT = 1.5;
    img = fem_model_arms( pp) ;
    x_pts = linspace(-5,15,301)/100;
    y_pts = linspace( 0,45,201)/100;
    msm = struct('level',[inf,-.05,inf], ...
          'x_pts', x_pts, 'y_pts', y_pts);

    img.fwd_model.stimulation =  ...
       stim_meas_list(pp.SSMM,num_elecs(img));
    img.fwd_solve.get_all_nodes = true;
    vh = fwd_solve(img);
    imgr = rmfield(img,'elem_data');
    imgr.node_data = vh.volt;
    imgr.fwd_model.mdl_slice_mapper = msm;
%   Filt = ones(3)/9; Filt = conv2(Filt,Filt);
    %imgr.calc_slices.filter = Filt;
    imgr = calc_slices(imgr);


    hold on
    for fi=1:2; % Horrible to fight matlab 
        FakeVol(fi,:,:) = permute(imgr,[3,2,1]);
    end
    hh=contourslice(x_pts, [-.15,.15],y_pts,  ...
        FakeVol, [],-.051,[], pp.contours);
    for hi = hh;
        set(hh,'EdgeColor', [.2,.3,.2],'LineWidth',2);
    end
    hold off;

