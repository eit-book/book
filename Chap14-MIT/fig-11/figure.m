%frequency [kHz]
%normalized conductivity inductive
%normalized conductivity electrode
%error 1
%error 2
data = [ ...
 10, NaN, 0.26, NaN ,NaN
 20, NaN, 0.32, NaN ,NaN
 30, NaN, 0.41, NaN ,NaN
 40, 0.57, 0.51, 0.37, 0.74,
 50, 0.75, 0.61, 0.49, 0.92,
 60, 0.8, 0.72, 0.67, 0.92,
 80, 0.96, 0.96, 0.92, 0.98,
120, 1.35, 1.42, 1.31, 1.43,
150, 1.85, 1.85, 1.82, 1.9,
200, 2.42, 2.42, 2.37, 2.48,
250, 3.05, 2.94, 2.94, 3.1,
300, 3.4, 3.38, 3.32, 3.54,
370, 3.7, 3.89, 3.59, 3.9,
400, NaN, 4.1, NaN, NaN
500, NaN, 4.63, NaN, NaN
600, NaN, 5.1, NaN, NaN
700, NaN, 5.4, NaN, NaN
800, NaN, 5.65, NaN, NaN
900, NaN, 5.85, NaN, NaN
1000, NaN,6, NaN, NaN
];
f= data(:,1);
lw = {'LineWidth',1,'Color',[0,0,0]};
clf;subplot(4,3,[1,2,4,5])
semilogx(f,data(:,3),lw{:})
hold on;
e1 = abs(data(:,2)-data(:,4));
e2 = abs(data(:,2)-data(:,5));
errorbar(f,data(:,2),e1,e2,'.','MarkerSize',10,lw{:});
box off
hold off
ylabel('normalized conductivity')
xlabel('frequency [kHz]')
print -dpdf figure.pdf
