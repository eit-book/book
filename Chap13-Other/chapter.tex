\ifx\CURRCHAP\undefined \def\BUILDCHAPTERALONE{TRUE} \fi
\ifdefined\BUILDCHAPTERALONE
 \def\CURRCHAP{.} \input{../includes.tex}
 \begin{document}
 \setcounter{chapter}{13}\addtocounter{chapter}{-1} 
\fi

\chapter{Other clinical applications of EIT}
\label{ch13:main}
\chapterauthor{Ryan Halter and     David Holder}
              {Ryan Halter$^1$ and David Holder$^2$}
\chapteraffil{%
$^1$Thayer School of Engineering, Dartmouth College, Hannover, NH, USA
\newline
$^2$Medical Physics and Bioengineering, University College London, UK
}


The principal clinical applications for biomedical EIT are imaging of heart and
lung function in the thorax, gastric\index{gastric} emptying, soft-tissue cancers\index{cancer} and brain\index{brain}
function. These are all covered by individual chapters elsewhere in this
volume. There are several other possible applications, some of these of
historical interest -- they were started in the first flush of enthusiasm when the
Sheffield mark 1 system became available in the mid 1980s, but then active
research was discontinued because of inherent technical problems, or because
other areas within EIT appeared more promising. Recently there has been a
flurry of new activity in exploring new and revisiting older clinical
applications that are being made possible with the availability of smaller
form-factor IC-based EIT systems, cost-effective research-based EIT systems
that have entered the market, and increased computational power.

While these other clinical applications have not yet been fully optimized\index{optimize} nor
vetted in clinical trials, both the historical applications and emerging
applications are reviewed in this chapter to provide insight into what might be
possible with EIT. 

\section{Tumour Ablation Monitoring}

Malignant tumours may be treated by artificially increasing temperature\index{temperature} by
radiofrequency\index{radiofrequency}, microwave or laser ablation approaches or by decreasing
temperature\index{temperature} using cryoablation\index{cryoablation}. It is essential to monitor tissue temperature\index{temperature}
during these procedures so that normal tissue is not overheated or overcooled,
while malignant tissues are heated to the desired temperature of about \SI{50}{\celsius} or
cooled to temperatures of about \SI{-40}{\celsius} for a few minutes. At present, this is
achieved by inserting thermocouples into the tumour. This is practicable for
superficial tumours, but difficult for deep ones. MRI-based thermometry has
been proposed as an alternative  method for monitoring ablation of deeper
tumours, but this approaches requires specialized MR-compatible ablation probes
and a high-cost interventional MR. There is therefore a need for an accurate
non-invasive\index{non-invasive} thermometry method that can easily be applied and work well for
both superficial and deep tumours. In principle, EIT might be suitable for
this, because there is a nearly linear monotonic relation between temperature
and impedance\index{impedance} change in simple aqueous solutions—the impedance\index{impedance} of ionic
solutions varies inversely with temperature by about 2\% per \si{\celsius} 
\cite{Griffiths1987Dual}.
 EIT therefore presents a possible non-invasive means of imaging
temperature within a subject. 

Unfortunately, the relationship between resistivity and temperature is complex.
Using a laser probe to heat ground calf liver in a cylindrical tank,
Möller \etal \cite{Moeller1993Electrical}
 compared changes within the EIT image with temperature determined by
thermocouples. The tissue was heated to between \SI{35}{\celsius} and
\SI{60}{\celsius} using an oscillation inducing thermoregulatory feedback system.
There was a qualitative correlation between changes in the EIT image and
temperature, but a substantial impedance\index{impedance} drift of uncertain origin occurred. A
similar study was performed in a tank filled with conducting agar, into which
small pieces of foam had been inserted in order to simulate inhomogeneous
tissue. Heating was performed with radiofrequency\index{radiofrequency} coils\index{coil} \cite{Conway1992Experimental}.
A linear relation was observed between EIT image changes and temperature, but the
slopes varied with position in the phantom. 

Temperature calibration\index{calibration} experiments have also been performed in vivo. In three
volunteers, 200\,ml of conducting solutions at various temperatures were
repeatedly introduced into the stomach, whilst EIT data was acquired from
electrodes\index{electrode} around the abdomen and conductivity images reconstructed
\cite{Conway1992Experimental}.
Acid production was suppressed by cimetidine. It was found necessary
to compensate for baseline drifts in the images. After compensation, a linear
relationship between the temperature of the infused fluid and region of
interest integral was observed, although the slopes varied between subjects. 

Unfortunately, reliable clinical use for hyperthermia monitoring requires a
high degree of both spatial and contrast\index{contrast} resolution. Single images in the thigh
\cite{Griffiths1987Dual}
 and over the shoulder blade
\cite{Conway1987Electrical}
 of human
subjects, with the Sheffield mark I system, during warming showed substantial
artefacts, and it was also demonstrated in normal volunteers, without warming,
that baseline variability would produce impedance\index{impedance} changes which were equivalent
to temperature changes of several degrees. Pilot clinical measurements made in
the mid-1990s with planar arrays at 12.5\,kHz showed encouraging average results,
but estimates of some of the tissue temperatures were erroneous by \SI{9}{\celsius}
\cite{Moskowitz1995Clinical, Paulson1992Electrode}.

There has been an increased interest in incorporating a priori\index{a priori} information from
other diagnostic imaging modalities to improve the EIT reconstruction\index{reconstruction} outputs
in hyperthermia monitoring applications. One such approach uses a coupled
electromagnetic (Gauss’s Law) and heat transfer (Pennes’ bioheat equation)
model to describe the coupled biophysical processes in action during
EIT-monitoring of RF-ablation
\cite{Ferraioli2009Effective}.
In this approach, MR
images are used to construct an anatomically accurate mesh with internal organ
segmentation in order to specify organ-specific electrical and thermal
properties within in the mesh. The RF probe is used in this case as a current
drive with a single skin-located return electrode. This approach has been
extended by adding a multiplicity of external return electrodes\index{electrode} making this a
more feasible configuration for EIT reconstruction\index{reconstruction}
\cite{Caminiti2010Adaptive}.
While
these approaches may be promising, their implementation has been limited to
numerical exploration to date.

More recently, advanced EIT data acquisition systems have been used to capture
temperature and tissue property changes of ex vivo bovine liver undergoing
radiofrequency\index{radiofrequency} (RF) ablation 
\cite{Wi2015Real}.
EIT images were acquired at
10\,kHz and 100\,kHz and both time- and frequency-difference images were evaluated.
RF application and EIT data acquisition were duty-cycled (18\,s RF / 2\,s EIT) to
ensure no RF interference in the EIT data and temperature was recorded via
thermocouples during EIT data acquisition. Both time and frequency difference
images showed significant conductivity changes associated with ablation regions
as compared to unablated regions. It is suggested that while EIT can accurately
detect ablation regions, the accuracy and resolution of the reconstruction\index{reconstruction} is
limited making clinical utility and translation a challenge.

In the case of EIT-based cryomonitoring, feasibility experiments were initially
explored using ex vivo chicken breast and liver tissues
\cite{Otten2000Cryosurgical}.
Tissue samples subjected to cryoablation\index{cryoablation} were positioned directly on a set of
linearly spaced electrodes\index{electrode} and a developing ice front was detectable to
approximately half the spacing between the sense electrodes\index{electrode}. Positioning
electrodes\index{electrode} local to the ablation site in clinical applications are an obvious
challenge, though co-located impedance\index{impedance} sensing probes with the cryodelivery
probe is a potential solution to this. In one such implementation, a cryoprobe
instrumented with 16 electrodes\index{electrode} (two rows of 8 electrodes\index{electrode}) was submerged into a
saline-filled tank with 16 peripherally positioned electrodes\index{electrode} (i.e.\
representing skin electrodes\index{electrode} adhered around the abdomen of a patient)
\cite{Hartov2002Using}.
Ice balls were formed in the tank while impedance\index{impedance} was acquired
from probe and peripheral electrode combinations. A dual mesh reconstruction\index{reconstruction}
algorithm with both peripheral and internal electrodes\index{electrode} was used to demonstrate
the feasibility of imaging ice ball formation. Significant improvements in
imaging the internal ice ball were observed when both probe and peripheral
electrodes\index{electrode} were used as opposed to peripheral electrodes\index{electrode} only. 

Additional numerical simulations that explore the use of secondary imaging
modalities to serve as a priori\index{a priori} information for guiding EIT reconstruction\index{reconstruction} in
cryoablation\index{cryoablation} have also been suggested. Specifically, use of ultrasound to
define a specific region of interest over which the electrical properties are
estimated has been proposed as a way to decrease the ill-posedness\index{ill-posed} of the
reconstruction\index{reconstruction} algorithm
\cite{Edd2006Detecting}. As an extension of this work, a
level-set\index{level-set} method that uses data from multiple narrow-band ultrasound
transducers to guide boundary shape estimation of the ice ball has been
proposed to improve ice front monitoring 
\cite{Soleimani2006Electrical}.

In addition to monitoring during ice ball formation, it has been suggested that
acquiring a pre-ablation and post-thaw EIT images (i.e. after the ablated
tissue has completely returned to normal body temperature) may serve to better
assess the actual volume of tissue that was effectively ablated. In this case,
difference impedance\index{impedance} images would serve to map the treated tissue based on
cellular morphology post-ablation rather than acting as a thermometry system to
map the temperature distribution as is the case in monitoring during ablation. 

In all of these ablation monitoring applications, accurate temperature
estimation requires not only accurate imaging, but also an assumed linear (or
at least known) relationship between temperature and conductivity. This latter
appears to change in a hysteretic fashion during tissue heating. Given this
uncertainty in calibration\index{calibration} a priori\index{a priori}, and the baseline variability in vivo, it
seems that EIT may struggle to be an accurate technique unless substantial
improvements in system performance continue to be made
\cite{Blad1992Quantitative, Paulson1992Electrode}.
Improvements including coupled electromagnetic-heat
conduction models and use of a priori\index{a priori} imaging to better constrain the
reconstruction\index{reconstruction} algorithm may help in this effort. Likewise, incorporating both
probe-located electrodes\index{electrode} and skin-based surface electrodes\index{electrode} is likely to provide
better sensitivity\index{sensitivity} local to the ablation site. In vivo animal model
measurements in which blood perfusion\index{perfusion} is present are critically needed to
better evaluate the potential EIT-based ablation monitoring in a more realistic
clinical setting. 



\section{System-on-chip and cell/tissue imaging}

With the advancements in micromanufacturing and system-on-chip technologies,
there has been an increased interest in exploring the use of EIT for in vitro
cell imaging. Cell studies typically require destructive staining processes to
image and evaluate cellular response; EIT represents a non-invasive,
non-destructive modality that can provide real-time\index{real-time} feedback on cellular
response to different stimuli (i.e. cell growth studies, cancer\index{cancer} treatment
studies, etc.).  Specific, potential applications of cellular imaging include
high throughput screening of blood\index{blood} samples to detect circulating tumor cells,
pharmaceutical studies of drug efficacy, cell growth studies and non-invasive
cell aggregation and health monitoring during tissue manufacturing. 

Chen \etal \cite{Chen2012CMOS}
developed a CMOS-based 96$\times$96 microelectrode
array with each square electrode having a \SI{25}{\micro\meter} edge length
(approximately the size of a single cell).
A single counter electrode was positioned above the
array and individual electrode impedances were recorded from each electrode
using a benchtop LCR-meter. While tomography\index{tomography} was not performed, impedance\index{impedance} maps
were generated to show cell density and to perform cell counting with each
electrode serving as a pixel\index{pixel}. 

In another example, a more traditional circular EIT configuration was
implemented on a printed circuit\index{circuit} board (PCB) using standard photolithography
techniques \cite{Yang2016Miniature}.
A 10\,mm cylindrical tank adhered around 16
microelectrodes printed on the PCB served as a vessel for imaging breast cancer\index{cancer}
cell spheroids and high-density cell pellets. While the electrodes\index{electrode} were only
present on the surface of the PCB, 12-layer 3D conductivity images were
successfully reconstructed. This system has been further used for hydrogel,
scaffold, and tissue imaging for use in tissue manufacturing applications
\cite{Wu2018Exploring, Wu2019Calibrated}.

These are nice early examples of what can be achieved with cellular EIT
imaging. A good review of electrical impedance\index{impedance} methods in general for cell
characterization can be found in Schwarz \etal \cite{Schwarz2020Spatially}.

\section{Wearables}

The increased availability of ASIC\index{ASIC}-based EIT systems
(e.g.{} ~\cite{Guermandi2014Active, 
     Lee2020Nine, Liu2019Thirteen, 
     Rao2018Analog, Rao2020One,
     Takhti2018Ten, Triantis2011Multi, Wu2019Calibrated}
and other off-the-shelf IC-based EIS sensing solutions (e.g.{} AD5933 (Analog
Devices), AFE4300 (Texas Instruments)) make developing small form-factor,
low-powered EIT systems possible. While the clinical applications for which
these small form-factor systems will have utility still needs to be determined,
early devices have demonstrated proof-of-concept. The below summary includes
only systems that are fully wireless and wearable in the sense that a patient
would not be tethered to an off-person device. 

Hand gesture recognition has been envisioned as once such application. Tomo is
a wrist-wearable, eight electrode EIT system designed for detecting various
hand gestures \cite{Zhang2015Wearable}.
 The idea is that conductivity images of the
muscle activation and muscle, tendon, and bone movement in the wrist will be
unique for different hand gestures. This wearable design leverages the AD5933
and a pair of 8-to-1 multiplexors for electrode selection. Difference images
are computed between measurements recorded at a relaxed state and those
recorded for a specific gesture.  Accuracies as high as 97\% were reported for
whole hand gestures when trained on a single individual. 

Jiang \etal \cite{Jian2020Hand} extended this work to include two rows of eight
electrodes\index{electrode} positioned along the forearm. A custom EIT ASIC\index{ASIC} and analog switch
matrix was used in this case. They were able to achieve a gesture
classification accuracy of 97.9\% when a single band of 8 electrodes\index{electrode} is used and
an accuracy of 99.5\% when two rows of 8 electrodes\index{electrode} are used. 

Another recent device developed by Yao et al \cite{Yao2019Development}
used the Red Pitaya platform, a voltage controlled current source and set of
multiplexors to serve as a wrist wearable device. They introduced conductive
cloth electrodes\index{electrode} with sponge filling to help improve constant electrode
contact. This team found a gesture recognition accuracy of 95\% in their study.  

Finally, Wu \etal \cite{Wu2018Human}
introduced an ASIC\index{ASIC}-based forearm wearable
EIT system for gesture detection. However, in addition to gesture detection
alone they propose using this as a human-machine interface that enables control
of a hand prosthesis through use of EIT images acquired of the forearm. They
found gesture classification accuracies of 94.4--98.5\% depending on the number
of gestures they were classifying. 

It is important to emphasize that all data collected in these gesture
classification studies was acquired in a controlled laboratory setting from
small numbers of users (2--10 study participants); in a real-world scenario
electrode movement, electrode-skin interface conditions, and general motion
artifact are expected to decrease overall accuracy. Also, because all systems
used machine learning based classification, it is not specifically clear what
the value of tomographic image reconstruction\index{reconstruction} is in this scenario. 

Thoracic imaging with wearable EIT systems have also been developed, however
the majority of these still require tethering to an off-person power supply or
data acquisition system. In one example of an untethered system, an MSP430
(Texas Instrument) microcontroller coupled to a voltage-to-current\index{voltage-to-current} converter,
an amplifier\index{amplifier} and demodulation\index{demodulation} circuit\index{circuit}, and a multiplexing front end were
interfaced to 16 electrodes\index{electrode} \cite{Huang2016Design}.
 An on-board Bluetooth module
was used to transmit data wireless to a host laptop for image reconstruction\index{reconstruction}
and analysis. Low resolution imaging of lung inhalation and exhalation was
demonstrated, though the actual clinical application for continuous ambulatory
thoracic imaging was not discussed. 

\section{Intra-Pelvic Venous Congestion}

Pooling and congestion of blood\index{blood} in the pelvis is a poorly understood phenomenon
which is thought to be the cause of pelvic discomfort in women. Thomas et al
(1991) investigated the possible use of EIT in its diagnosis, on the basis that
abnormal pooling would produce impedance\index{impedance} changes. EIT images were collected
with a ring of electrodes\index{electrode} around the pelvis, as the subject was placed in
horizontal and vertical positions using a tilt table. The rationale was that
this should produce fluid shifts in the pelvis. A central area of impedance\index{impedance}
change was observed in both normals and subjects, with pelvic congestion
diagnosed by venography. A significant difference in the ratio of the areas
anterior and posterior to the coronal midline and greater than 10\% of the peak
impedance\index{impedance} change was observed. No difference in mean amplitude of impedance\index{impedance}
changes was observed between the two groups. Venography is an invasive
procedure, so EIT would provide a welcome alternative. However, there is no
direct evidence concerning the origin of these changes, although it has been
shown that they are at least plausible by comparison with EIT images made in
tanks with saline-filled tubing \cite{Thomas1994Correction}. This is an intriguing and
potentially valuable application, but larger prospective studies will be needed
before its use can be established.  


\section{Other potential applications}

Using a 16 electrode system operating at 10\,kHz and an algorithm similar to
that of the Sheffield system,
\cite{Kulkarni1989Aberdeen, Kulkarni1990Impedance}
were able to produce
EIT images in long bones. Areas of increased resistivity could be identified in
the normal subject and 16 weeks after fracture, whilst a similar region showed
lower resistivity in another subject, four weeks after fracture (Ritchie et al,
1989). It remains to be determined if such results could be used effectively to
monitor fracture healing. However, fractures can at present be assessed with
great accuracy by X-ray\index{X-ray}. EIT might offer an advantage if repeated measurement
was needed for follow-up, but it is unlikely that it could offer appropriate
spatial resolution. 

Other proposed applications have included EIT imaging of limb plethysmography\index{plethysmography}
\cite{VonkNoordegraaf1997Validity},
apnea\index{apnea} monitoring
\cite{Lee2018Portable, Woo1992Measuring},
intra-abdominal bleeding or fluid pooling
\cite{Sadleir2001Detection, Shuai2009Application, Tucker2010Invivo},
neuromuscular disease \cite{Murphy2018Toward},
vesicoureteral reflux \cite{Dunne2018Detection} but no direct evidence is yet
available to assess the likely clinical accuracy of these possibilities. 







\ifdefined\BUILDCHAPTERALONE
 \bibliography{../bib/book} \bibliographystyle{ieeetr}
 \end{document}
\fi
