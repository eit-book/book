% RPI tank model $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

Nel= 32; %Number of elecs
Zc = .0001; % Contact impedance
curr = 20; % applied current mA


th= linspace(0,360,Nel+1)';th(1)=[];
els = [90-th]*[1,0]; % [radius (clockwise), z=0]
elec_sz = 1/6;
fmdl= ng_mk_cyl_models([0,1,0.1],els,[elec_sz,0,0.03]);

for i=1:Nel
   fmdl.electrode(i).z_contact= Zc;
end

% Trig stim patterns
th= linspace(0,2*pi,Nel+1)';th(1)=[];
for i=1:Nel-1;
   if i<=Nel/2;
      stim(i).stim_pattern = curr*cos(th*i);
   else;
      stim(i).stim_pattern = curr*sin(th*( i - Nel/2 ));
   end
   stim(i).meas_pattern= eye(Nel)-ones(Nel)/Nel;
   stim(i).stimulation = 'Amp';
end

fmdl.stimulation = stim;


% load RPI tank data $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

load Rensselaer_EIT_Phantom;
vh = real(ACT2006_homog);
vi = real(ACT2000_phant);

if 1
% Preprocessing data. We ensure that each voltage sequence sums to zero
  for i=0:30
    idx = 32*i + (1:32);
    vh(idx) = vh(idx) - mean(vh(idx));
    vi(idx) = vi(idx) - mean(vi(idx));
  end
end

% RPI tank model $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

% simple inverse model -> replace fields to match this model
imdl = mk_common_model('b2c2',32);
imdl.fwd_model = mdl_normalize(imdl.fwd_model, 0);

imdl.fwd_model.electrode = imdl.fwd_model.electrode([8:-1:1, 32:-1:9]);

imdl.fwd_model = rmfield(imdl.fwd_model,'meas_select');
imdl.fwd_model.stimulation = stim;
imdl.hyperparameter.value = 1.00;

% Reconstruct image
img = inv_solve(imdl, vh, vi);
img.calc_colours.cb_shrink_move = [0.5,0.8,0.02];
img.calc_colours.ref_level = 0;
clf; show_fem(img,[1,1]); axis off; axis image

% RPI tank model $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

% simple inverse model -> replace fields to match this model
imdl.fwd_model = fmdl; 

img = inv_solve(imdl , vh, vi);
img.calc_colours.cb_shrink_move = [0.5,0.8,0.02];
img.calc_colours.ref_level = 0;
clf; show_fem(img,[1,1]); axis off; axis image

% RPI tank model $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

% In 3D, it's important to get the model diameter right, 2D is
imdl.fwd_model.nodes= imdl.fwd_model.nodes*15; % 30 cm diameter

% Estimate the background conductivity
imgs = mk_image(imdl);
vs = fwd_solve(imgs); vs = vs.meas;

pf = polyfit(vh,vs,1);

imdl.jacobian_bkgnd.value = pf(1)*imdl.jacobian_bkgnd.value;
imdl.hyperparameter.value = imdl.hyperparameter.value/pf(1)^2;

img = inv_solve(imdl, vh, vi);
img.calc_colours.cb_shrink_move = [0.5,0.8,0.02];
img.calc_colours.ref_level = 0;
clf; show_fem(img,[1,1]); axis off; axis image


% Absolute reconstructions $Id: build_figure.m,v 1.1 2021/09/30 20:01:36 adler Exp $

imdl = mk_common_model('b2c2',32);
imdl.fwd_model = fmdl;
imdl.reconst_type = 'absolute';
imdl.hyperparameter.value = 2.0;
imdl.solve = @inv_solve_abs_GN;

residual = [ 246291; 59975.8; 3226.97; 55.7424;
   30.7801; 30.7091; 30.7019; 30.6998;];

k=0;for iter = [1,2, 8]; k=k+1; subplot(2,3,k);
   imdl.inv_solve_gn.max_iterations = iter;
   imdl.inv_solve_gn.dtol = 0;
   img = inv_solve(imdl , vi);
   img.calc_colours.cb_shrink_move = [0.5,0.8,0.02];
   img.calc_colours.ref_level = 0.6;
   img.calc_colours.clim = 0.6;
   hh=show_fem(img); axis off; axis image
   set(hh,'EdgeAlpha',0);
   title(['iter = ',num2str(iter)'])

end
subplot(3,1,3);
semilogy(residual,'.-','LineWidth',2); box off
ylim([1,1e6]); ylabel('|| v - F(x) ||^2')
xlabel('iteration')
print_convert('RPI_image.jpg');
residual([1,2,8])'./ [346.3142 78.5272 6.0004]

hplist = logspace(-2,2,20);
imdl.inv_solve_gn.max_iterations = 4;
img0= img;
for i = 1:length(hplist)
   imdl.hyperparameter.value = hplist(i);
   img = inv_solve(imdl , vi);
   dn(i) = norm(getfield(fwd_solve(img),'meas')-vi);
   in(i) = norm(img.elem_data'*calc_RtR_prior(imdl)*img.elem_data);

   img0.elem_data(:,i) = img.elem_data;
end
clf
subplot(211);
img0.calc_colours.backgnd = [1,1,1];
img0.calc_colours.greylev = -0.1;
fs =  [2:3:20];
img0.get_img_data.frame_select = fs;
img0.show_slices.img_cols = length(fs);
show_slices(img0);

subplot(313);
dx = [dn;in]';
loglog(hplist,dx,'LineWidth',2);
box off
hold on
loglog(hplist(fs),dx(fs,:),'o','LineWidth',2,'Color',[0,0,0]);
hold off
legend('||V-F(x)||','||L(x-x_{res})||')


print_convert('norm_image.jpg');
