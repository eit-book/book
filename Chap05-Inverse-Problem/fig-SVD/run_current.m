if 0
fmdl= ng_mk_cyl_models([1,1],[32,.5],[0.08]);
fmdl.nodes = fmdl.nodes - [0,0,0.5];
else
fmdl= getfield(mk_common_model('d2C2',32),'fwd_model');
end
img = mk_image(fmdl,1);

clf
subplot(241);
   optf.viewpoint = struct('az',-05,'el',40);
   optf.edge.width = 0;
%  show_fem_enhanced(fmdl,optf);

subplot(2,1,1);
 hold on;
for pl = 1:2; switch pl
   case 2; SKIP = [0,1];
   case 1; SKIP = [0,5];
   end
   img.fwd_model.stimulation = mk_stim_patterns(32,1,SKIP,SKIP,{'meas_current'},1);
   J= calc_jacobian(img);
   [U,D,V]= svd(J);
   semilogy(diag(D),'LineWidth',3); box off
   xlim([1,500]); ylim([1e-15,1])
end
hold off; set(gca,'YScale','log')
legend('Skip=0','Skip=4')
axes('position',[ 0.14  0.60  0.76  0.2]);

V = V./get_elem_volume(img)';
V = V./max(abs(V));
imgr= mk_image(fmdl,V);
imgr.calc_colours.backgnd = [1,1,1];
imgr.calc_colours.npoints = 128;
imgr.calc_colours.greylev = -.1;
fs = [10,50,60,80,100,110,150,200,250];
imgr.get_img_data.frame_select= fs;
imgr.show_slices.img_cols = length(fs);
show_slices(imgr,[inf,inf,0])
print -dpdf fig_SVD.pdf
