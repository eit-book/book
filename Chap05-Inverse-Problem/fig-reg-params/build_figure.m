extra={'ball',['solid ball = ' ...
               ' orthobrick( 0.25,-0.2,0; 0.35, 0.4,0.5) or ' ...
               ' orthobrick(-0.35,-0.2,0;-0.25, 0.4,0.5) -maxh=0.03;']};

Nel = 16;

fmdl= ng_mk_cyl_models(0,[Nel],[0.1,0,0.05],extra); 
[stim,msel] = mk_stim_patterns(Nel,1,[0,5],[0,5],{},1);
fmdl.stimulation = stim;
img= mk_image(fmdl, 1 );             vh=fwd_solve(img);
img.elem_data(fmdl.mat_idx{2}) = 2;  vi=fwd_solve(img);

if 0
subplot(241); hh=show_fem(img); set(hh,'EdgeAlpha',0.1);
axis off;
end


randn('seed',0);
hp = 1e-3*[ ...
       1.65,  15.6,  2.7,   622, .01;
       0.118,  1.24, 0.175,  44, .0001];
for j=0:1;
   imdl= mk_common_model('d2C2',Nel);
   imdl.fwd_model.stimulation = stim;
   imdl.hyperparameter.tgt_elems = 1:4;
   for i=1:5; switch i
      case 1; imdl.RtR_prior = @prior_tikhonov;
              str = 'Tikhonov';
      case 2; imdl.RtR_prior = @prior_noser;
              str = 'Noser';
      case 3; imdl.RtR_prior = @prior_laplace;
              str = 'Laplace';
      case 4; imdl.solve = @inv_solve_TSVD;
              str = 'TSVD';
      case 5; imdl.solve = @inv_solve_TV_pdipm;
              imdl = rmfield(imdl,'RtR_prior');
              imdl.R_prior = @prior_TV;
              str = 'TV (pdipm)';
      end
      imdl.hyperparameter.value = hp(j+1,i);
%     calc_noise_figure(imdl);
      img = inv_solve(imdl, vh, vi);
      img.calc_colours.ref_level = 0;
      subplot(4,5,i+10*j);
      hh=show_fem(img); set(hh,'EdgeAlpha',0.1);
      axis off;
      if j==0; title(str); end
      subplot(4,5,i+10*j+5);
      hh=show_fem(img); set(hh,'EdgeAlpha',0.1);
      axis([-.2,.8,-.2,.8])
      axis off;
   end
end

print_convert tdEIT_reg_matrix.jpg -r150

