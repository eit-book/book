clf
extra = {'plus', ... 
   'solid plus = ellipsoid(-3,-3,6;2,0,0;0,2,0;0,0,3);'};
fmdl= ng_mk_ellip_models([10,10,8],[6,3,7],[1,0],extra); 
fmdl.nodes = fmdl.nodes(:,[2,1,3]);
fmdl.stimulation = mk_stim_patterns(12,1,[0,1],[0,1],{},1);

img = mk_image(fmdl,1);
vh= fwd_solve(img);
img.elem_data(fmdl.mat_idx{2}) = 1.1;
vi= fwd_solve(img);
img.calc_colours.clim = .1;
subplot(211);
   crop_model(gca, inline('x-z<-.8','x','y','z'))
   optf.viewpoint = struct('az',-05,'el',40);
   optf.edge.color = 0.5*[1,1,1];
   show_fem_enhanced(img,optf);

imdl = mk_common_model('b2c2',16);
imdl.fwd_model = fmdl;
xpl = 10; xpts = linspace(-10.6,10.6,xpl);
ypl =  9; ypts = linspace(-9.0,9.0,ypl);
[imdl.rec_model, imdl.fwd_model.coarse2fine] = ...
     mk_grid_model( imdl.fwd_model, xpts, ypts );
outside = find(sum(imdl.fwd_model.coarse2fine,1) < eps);
imdl.fwd_model.coarse2fine(:,outside) = [];
imdl.rec_model.coarse2fine(:,outside) = [];
rec_out = [2*outside-1,2*outside];
imdl.rec_model.coarse2fine(rec_out,:) = [];
imdl.rec_model.elems(rec_out,:) = [];

imdl.hyperparameter.value = 8e-3;
imdl.RtR_prior = @prior_tikhonov;
imgr = inv_solve(imdl,vh,vi);


 imdl.rec_model.nodes(:,3) = 0.5;
 imdl.rec_model.boundary = imdl.rec_model.elems;


subplot(211);

xx= xpts'*ones(1,ypl);
 xmm= xx(1:end-1,1:end-1); xmm= xmm(:);
 xmp= xx(1:end-1,2:end-0); xmp= xmp(:);
 xpm= xx(2:end-0,1:end-1); xpm= xpm(:);
 xpp= xx(2:end-0,2:end-0); xpp= xpp(:);
xpatch=[xmm,xmp,xpp,xpm]'; xpatch(:,outside)= [];
yy= ones(xpl,1)*ypts;
 ymm= yy(1:end-1,1:end-1); ymm= ymm(:);
 ymp= yy(1:end-1,2:end-0); ymp= ymp(:);
 ypm= yy(1:end-1,1:end-1); ypm= ypm(:);
 ypp= yy(2:end-0,2:end-0); ypp= ypp(:);
ypatch=[ymm,ymp,ypp,ypm]'; ypatch(:,outside)= [];
zpatch=0*xpatch + 5.0;

hh=patch(xpatch,ypatch, zpatch, NaN(size(xpatch,2),1));
set(hh,'FaceAlpha',0,'EdgeColor',[0,0,1],'lineWidth',4 );
xlim(12*[-1,1]); ylim(10*[-1,1]);
if 0
hold on
plot(xpts'*ones(1,ypl),ones(xpl,1)*ypts,'b');
plot(ones(ypl,1)*xpts,ypts'*ones(1,xpl),'b');
hold off
end

axis off
opt.resolution   = 250; 
print_convert('image_reconst1.jpg',opt); clf

%imgr.calc_colours.clim = 1.15;
imgr.calc_colours.ref_level = 0;
imgr.fwd_model = rmfield(imgr.fwd_model,'coarse2fine');
subplot(212);
% show_fem(imgr); axis off
   optf.viewpoint = struct('az',-00,'el',80);
   optf.edge.color = 0.5*[1,1,1];
   show_fem_enhanced(img,optf);
imgc = calc_colours(imgr);
hh=patch(xpatch,ypatch, zpatch, imgc); 
set(hh,'FaceAlpha',1,'EdgeColor',[0,0,1],'lineWidth',2,'CDataMapping','direct' );
axis off

%print -dpdf figure-draft.pdf
 print_convert('image_reconst2.jpg',opt);
