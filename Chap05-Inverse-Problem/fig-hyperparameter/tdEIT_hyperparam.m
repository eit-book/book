clf;
extra={'ball',['solid ball = ' ...
               ' orthobrick( 0.25,-0.2,0; 0.35, 0.4,0.5) or ' ...
               ' orthobrick(-0.35,-0.2,0;-0.25, 0.4,0.5) -maxh=0.03;']};

Nel = 16;
fmdl= ng_mk_cyl_models(0.5,[Nel,0.25],[0.1,0.2],extra); 
stim = mk_stim_patterns(Nel,1,[0,5],[0,5],{},1);
fmdl.stimulation = stim;
img= mk_image(fmdl, 1 );             vh=fwd_solve(img);
img.elem_data(fmdl.mat_idx{2}) = 2;  vi=fwd_solve(img);

subplot(1,8,[2,3]);
   optf.viewpoint = struct('az',0,'el',70);
   optf.edge.width = 0;
   show_fem_enhanced(img,optf);
axis off;
print_convert phantom.jpg -r250
clf;

imdl= mk_common_model('d2C2',Nel);
imdl.fwd_model.stimulation = stim;

randn('seed',0);
for j=0:1
   if j==1; vi = add_noise(100,vi,vh); end
   for i=2:4; switch i
      case 2; hp = 0.1;
      case 3; hp = 0.01;
      case 4; hp = 0.001;
      end
      imdl.hyperparameter.value = hp*0.1;
      img = inv_solve(imdl, vh, vi);
      img.calc_colours.ref_level = 0;
      subplot(2,5,1+i+5*j);
      hh=show_fem(img); set(hh,'EdgeAlpha',0.1);
      axis off;
      if j==0; title(sprintf('\\alpha = %1.3f',hp)); end
   end
end

%set(gcf,'PaperPosition',[1,1,8,3.5]);
%print -dpdf tdEIT_hyperparams.pdf
print_convert images.jpg -r250
