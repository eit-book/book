\ifx\CURRCHAP\undefined \def\BUILDCHAPTERALONE{TRUE} \fi
\ifdefined\BUILDCHAPTERALONE
 \def\CURRCHAP{.} \input{../includes.tex}
 \begin{document}
 \setcounter{chapter}{18}\addtocounter{chapter}{-1} 
\fi

\chapter{%
Veterinary applications of EIT
}
\label{ch18:main}
\chapterauthor{Martina Mosing     and Yves Moens}
              {Martina Mosing$^1$ and Yves Moens$^2$}
\chapteraffil{
$^1$
School of Veterinary Medicine, College of Science, Health, Engineering and Education, Murdoch University, Perth, Western Australia
\newline
$^2$
Division of Anaesthesiology and Perioperative Intensive Care, University of Veterinary Medicine Vienna,  Austria
}


\section{%
Introduction to Thoracic EIT in Veterinary Applications
}

The scientific development of thoracic EIT in humans relied on translational
research performed on experimental animals, especially swine. However, in the
veterinary field anesthetists were the first ones to recognize EIT's potential
for use in clinical research in other species such as horses, which are too
large for conventional lung\index{lung} imaging techniques. Active research in this field
began just over a decade ago. Today, research and clinical utilization of EIT
in veterinary medicine is still in its infancy, and veterinary-specific
literature is scant, in comparison to human medicine. 

\subsection{%
Creation of Finite Element Models for Animals }

Anatomically-correct reconstruction\index{reconstruction} of electrical impedance\index{impedance} tomography\index{tomography} (EIT)
images in different species necessitates the use of species-specific
finite element models\index{finite element models|seealso{FEM}} (\figref{fig:ch18:Animal-FEM}).
The routine method to calculate an average FE model is by using helical
computed tomography\index{tomography} (CT) scans during inspiratory hold. From the CT images
obtained at the electrode plane of the EIT belt, the heart, lungs and thoracic
contours are segmented. Segmented image files from several animals are then
used to create an average contour and finally the corresponding FE model for
that species. These FE models are then used to guide placement of the
electrodes\index{electrode} and create the final mesh for image reconstruction\index{reconstruction}
\cite{Waldmann2016Finite}.

Species-specific FE models have been created at the height of the 6th
intercostal space at half width of the thorax for beagle dogs and calves
respectively, as well as for pigs and ponies \cite{Waldmann2016Construction}.

In larger species that do not fit in CT scanners, alternative methods are
required to create FE models. One method is to construct models from
photographs taken of thoracic sections of anatomically dissected specimens.
This obtains the essential topographic anatomy, including the dimensions of the
lungs, heart and body-wall contour necessary to create the FE model. This has
been performed in a horse and a rhinoceros \cite{Brabant2018Construction}.
Another method used in live horses is to use gypsum to fabricate casts of the
thoracic contours. These shapes are subsequently digitized and averaged
including assumed organ location based on published anatomical studies
\cite{Ambrisko2016Assessment}.
These techniques are illustrated in \figref{fig:ch18:Animal-FEM}.


\begin{figure}[h]\centering
\includegraphics[width=\columnwidth]{\CURRCHAP/fig-FEM-creation/image1.png}
\FIGCAPTION{fig:ch18:Animal-FEM}{\CURRCHAP/fig-FEM-creation}
\end{figure}

\section{%
Translational Research in Animals
}

Similar to other new medical technologies, new EIT algorithms that show promise
are validated using animal models before they are released as clinical
applications in human medicine.

\subsection{Pig}

The pig is the most commonly used species for translational studies in EIT
research as they have a similar thoracic circumference for EIT belts to humans
(compared to rabbits and rodents), their topographic anatomy is relatively
similar to humans (compared to ruminants such as sheep), they grow relatively
quickly (compared to dogs) and there are relatively lower ethical concerns
(compared to primates). As of this writing, more than 175 papers have been
published on EIT in different pig models when using Pub Med search using
following search terms: (electrical impedance\index{impedance} tomography\index{tomography}) AND (pig OR swine OR
porcine). The majority of reports using pigs evaluate changes in EIT algorithms
in acute respiratory distress syndrome (ARDS\index{ARDS}) after induced lung\index{lung} injury. When
interpreting EIT ventilation data obtained from pig studies, the anatomical and
physiological differences between the human and porcine lung\index{lung} have to be
considered; pleura and the non-respiratory bronchioles show grossly the same
anatomical features, but the interlobular and segmental connective tissue, and
the alveolarized respiratory bronchioles, are different between the two species
\cite{Peake2015Connective}.
One important difference is that in humans, the interlobular connective
tissue only partially surrounds the lung\index{lung} lobules whereas in pigs this tissue
forms complete interlobular septa. This extensive inter-lobular connective
tissue prevents collateral ventilation between adjacent lobules in pigs
\cite{VanAllen1931Collateral}.
This difference must be acknowledged when extrapolating EIT data after specific
ventilatory interventions from pigs to humans as it is collateral ventilation
which partially prevents the development of lung\index{lung} collapse after distal airway
obstruction \cite{Robinson1982Some}.
 This makes the diseased human lungs less susceptible to
atelectasis\index{atelectasis} compared to pig lungs. These physiologic and anatomical differences
between species might result in unknown effects on pulmonary\index{pulmonary} function in
response to different external insults, which needs to be taken into
consideration when using EIT as a functional lung\index{lung} imaging technique. 

        




Pig models are also often used in cardiovascular\index{cardiovascular} studies in translational
research although pronounced differences in the anatomy of pulmonary\index{pulmonary} vessels,
muscular layers of arteries\index{artery} and location of pulmonary\index{pulmonary} veins exist between pigs
and humans. The latter should be considered when results from EIT studies using
pig models are used for EIT interpretation in humans \cite{Kay2015Blood}.
In particular, the pronounced hypoxic pulmonary\index{pulmonary} vasoconstrictor response present
in pigs \cite{Robinson1982Some} requires caution when the results of EIT lung\index{lung}
perfusion\index{perfusion} studies are extrapolated to humans. 

\subsection{Dog}

From an anatomical point of view dogs are the opposite of pigs. They have an
excellent collateral ventilation and minimal hypoxic pulmonary\index{pulmonary} vasoconstriction
\cite{Peake2015Connective}. This at least makes dog lungs functionally more comparable
with human lungs \cite{Woolcock1971Mechanical}. This also makes dog models more
reliable in translational research to monitor specific ventilatory
interventions using EIT. Dogs are not used extensively anymore in translational
research; however, partly because they are more difficult to breed and house
for research purposes compared to pigs, and partly due to ethical concerns.
Some early EIT papers, which describe the use of dog models and a 16 electrode
EIT system, showed that EIT can be used to estimate changes in tidal\index{tidal} volume,
lung\index{lung} inflation at the end of expiration\index{expiration} and liquid content in the lung\index{lung}
and acute pulmonary\index{pulmonary} edema\index{edema} 
\cite{Adler1997Monitoring, Adler1998Electrical, Newell1996Assessment}.
Ambrisko and co-workers used a dog model to evaluate the influence of
anesthesia\index{anesthesia} on the distribution of ventilation \cite{Ambrisko2017Impact}. They were
able to show that anesthesia\index{anesthesia}, but not recumbency, affected the centre of
ventilation and inhomogeneity factor in these dogs suggesting that this may
also apply in humans.

\subsection{Lamb and sheep}

Sheep are straightforward to house, breed and handle and are therefore often
used in translational research. Lambs have become an established animal model
for preterm lung\index{lung} research using EIT. Sheep usually have only one or two
offspring in comparison to pigs or dogs, where litter sizes are normally
larger. The construction of EIT belts for preterm lambs is less technically
challenging as their body size is comparable to the one of a human neonate.
Ethical concerns are also lower in lambs than in preterm non-human primates.
Tingay and colleagues used EIT monitoring to compare the effects of different
ventilation strategies used to inflate\index{inflated} the lungs of premature newborn lambs
\cite{Tingay2014Effect, Tingay2014Surfactant, Tingay2015Pressure,
Tingay2019Aeration}.
EIT was able to diagnose the
occurrence of a spontaneous pneumothorax for the first time \cite{Miedema2016First}, and even predict its occurance \cite{Miedema2019Electrical}.

The use and interpretation of thoracic EIT in adult sheep is challenging as the
rumen occupies a large volume on the left side of the body. The rumen is one
part of the multicompartmental stomach in ruminants and represents a gas filled
viscus adjacent to the diaphragm, partially covered by the ribcage. This can
interfere with the ventilation signal when the cranial\index{cranial} part of the rumen moves
into the EIT plane. The latter is even more likely in recumbent anesthetized
sheep as the natural escape mechanism for accumulating gas (eructation) is
disabled, causing ruminal tympany. Therefore, EIT data in sheep and other
ruminants have to be interpreted carefully. 
A sheep model has been used to investigate EIT as a monitoring tool to detect
pulmonary\index{pulmonary} embolism \cite{Nguyen2015Perfusion}. EIT images of lung perfusion\index{lung perfusion}
were created by injecting different concentrations of saline solution into the
right atrium. 

\subsection{Horse}

Given what is known about equine lung\index{lung} physiology and anatomy, horses are
potentially the best lung\index{lung} model for translational research.
 In both the human
and the horse, the separation of lung\index{lung} lobules is incomplete and collateral
ventilation and hypoxic pulmonary\index{pulmonary} vasoconstriction are comparable
\cite{Peake2015Connective}.
Furthermore, after induction of anesthesia\index{anesthesia} and recumbency, horses show
reproducible small airway collapse, atelectasis\index{atelectasis} and increases in venous
admixture within a short time \cite{Nyman1989Ventilation}. Therefore, different
ventilation strategies can be studied to re-inflate\index{inflated} these atelectatic\index{atelectatic} lung\index{lung}
regions without the confounding variables of inflammation and
bronchoconstriction\index{bronchoconstriction} which play a role in all other induced lung\index{lung}-injury animal
models.
Following up on all these arguments in favour of horses as a lung\index{lung} model, they
are not often used in translational research due to their size, which makes
their handling difficult and housing very expensive. 

One EIT paper evaluated the differences of 3D image reconstruction\index{reconstruction} in humans
and horses \cite{Grychtol2019Thoracic}. In both species, the data obtained from a
2-plane belt (2$\times$16 electrodes\index{electrode}) instead of the traditional one-plane 32
electrode belt showed anatomically plausible images. Image reconstruction\index{reconstruction} of
the horse data revealed a gas-filled organ ventral to the caudal lungs,
corresponding to the anatomical position of the large dorsal colon in horses.


\section{%
Clinical Research in Animals 
}

In this section, we review the use of EIT clinical research where the
primary goal
is to improve diagnosis and treatment of the animal, rather than as a
translational vehicle for human disease.

\subsection{Horses}
Most clinical research involving EIT in animals has been performed in horses.
This research was guided by the need to understand and treat
ventilation\index{ventilation}/perfusion\index{perfusion} mismatching and the resultant venous admixture which
occurs during anesthesia\index{anesthesia} in this species \cite{Nyman1989Ventilation}.
Until the
implementation of EIT into equine anesthesia\index{anesthesia} research, evaluation of the
pathophysiology of ventilation, and possible treatment, was difficult as
routine diagnostic imaging is very challenging (radiography, scintigraphy) or
impossible (CT or MRI) due to the size of the animal. The use of EIT in horses
has been made possible by the development of large custom-made rubber or
neoprene belts with metal electrodes\index{electrode} attached at equal distances.

\begin{figure}[h]\centering
\includegraphics[width=0.8\columnwidth]{./\CURRCHAP/fig-Horse+EIT/image2.png}
\FIGCAPTION{fig:ch18:EIT-horse}{./\CURRCHAP/fig-Horse+EIT}
\end{figure}


\begin{enumerate}
\item {\bf Distribution of ventilation}

The distribution of ventilation has been described using EIT in standing
unsedated horses \cite{Mosing2016Horses, Schramel2012Distribution}.
 \Figref{fig:ch18:EIT-horse} shows
collection of EIT data in a standing horse. The effect of pregnancy on the
distribution of ventilation was studied in Shetland ponies before and after
parturition \cite{Schramel2012Distribution}.
 The results confirmed that the increasing
size of the uterus influences the distribution of ventilation by shifting
ventilation towards dorsal lung\index{lung} areas and that this distribution abruptly
normalizes after birth. 

A feasibility study to evaluate the distribution of ventilation by evaluation
of the fEIT\index{fEIT} parameters, left-to-right ratio, centre of ventilation and global inhomogeneity\index{inhomogeneity}
index was performed in standing horses \cite{Ambrisko2016Assessment}.
Functional EIT
images were created using standard deviations (SD) of pixel\index{pixel} signals and
correlation coefficients (R) of each pixel\index{pixel} signal compared with a reference
respiratory signal. Different interventions (sighs, CO$_2$-rebreathing and
sedation) coupled to plethysmographic\index{plethysmography} spirometry\index{spirometry} were used to confirm
feasibility of EIT to detect changes in ventilation. An interesting finding was
that an inverse respiratory signal was found in the most ventral dependent
region of the EIT image. This is thought to be due to the cyclic appearance of
a gas-filled abdominal organ (most likely the dorsal large colon) into the EIT
plane at the 4-5th intercostal space. This signal should not be interpreted as
being of lung\index{lung} and needs to be distinguished from heart origin when detailed
analysis of the thoracic EIT signal in the horse is required \cite{Mosing2020What}.

The breathing pattern of horses and the regional distribution and dynamics of
ventilation were also evaluated prior to and following recovery from general
anesthesia\index{anesthesia} in dorsal (supine) recumbency \cite{Mosing2016Horses}. To determine
regional time delays within the lungs, the inflation period of seven regions of
interest (ROIs\index{ROI}) evenly distributed along the dorso-ventral axis of the lungs
was calculated. The regional filling time was defined as the time at which
$\Delta Z(t)$ of each region reached 50\% of its maximum impedance\index{impedance} change during
inspiration\index{inspiration}, normalized by the global inspiratory time. The regional inflation
period was defined as the time period during which $\Delta Z(t)$ of each lung\index{lung} region
remained above 50\% of its maximum inspiratory impedance\index{impedance} change, normalized by
the global breath length. After recovery of anesthesia\index{anesthesia} in dorsal recumbency a
respiratory pattern with inspiratory breath-holding was observed for six hours
after standing. During these episodes, EIT showed that ventral lung\index{lung} areas were
emptying while the dorsal areas were still filling, suggesting redistribution
of air from ventral into dorsal lung\index{lung} regions. This is considered an
auto-recruitment mechanism of lung\index{lung} tissue in areas which were dependent, and
likely atelectatic\index{atelectatic}, during anesthesia\index{anesthesia}. 

Several studies have specifically addressed the distribution of ventilation in
horses during the anesthetic period 
\cite{
 Ambrisko2017Assessment,
 Auer2019Monitoring,
 Mosing2016Horses,
 Mosing2017Regional,
 Mosing2018Regional,
 Mosing2019Monitoring}.
In mechanically ventilated ponies in right lateral recumbency, the centre of
ventilation (CoV) was in the non-dependent left lung\index{lung} \cite{Auer2019Monitoring}.
A shift of CoV from the dependent to the less perfused non-dependent areas of the lungs
was also shown after initialisation of volume-controlled\index{volume-controlled} ventilation (CMV) in
dorsally recumbent anesthetized horses \cite{Mosing2017Regional}.
 The initiation of
CMV was accompanied by an increase in silent spaces and a decrease in
ventilation in the dependent lung\index{lung} areas suggesting additional atelectasis\index{atelectasis}
development. This may be one of the reasons why a lack of improvement in
oxygenation is frequently seen when switching from spontaneous to controlled
ventilation in anesthetized horses. EIT has also
been used to show the effect of specific ventilation interventions; one case
report evaluated the recruitment of collapsed lung\index{lung} areas in the dependent lung\index{lung}
under EIT guidance. The changes in ventilation distribution were measured in
real time \cite{Moens2014Variety}. Regional lung\index{lung} compliance\index{compliance} was calculated by
simultaneous EIT and airway pressure measurements. This case report also
discussed preliminary perfusion-related\index{perfusion-related} information from the EIT signal.
Changes in a ventilation\index{ventilation}/perfusion\index{perfusion} mismatch algorithm based on the EIT signal
was compared to changes in blood gas analysis and showed promise for EIT-based
evaluation of ventilatory interventions for V/Q mismatch. 

In anesthetized horses, as in humans, recruitment manoeuvres or continuous
positive airway pressure (CPAP\index{CPAP}) can be used to re-inflate\index{inflated} collapsed alveoli or
to maintain alveolar\index{alveolar} patency, respectively -- a process that can be demonstrated
and confirmed by EIT \cite{Ambrisko2017Assessment, Mosing2018Regional}.
As described in
the human literature, the simultaneous recording of EIT signal and airway
pressure allows the evaluation of regional lung\index{lung} compliance\index{compliance}. A positive
relationship between increases in dependent lung\index{lung} compliance\index{compliance} following
recruitment manoeuvres and blood oxygenation\index{oxygenation} has been found \cite{Ambrisko2017Assessment}.
 In the CPAP\index{CPAP} study, the CoV shifted to dependent parts of the lungs
indicating a redistribution of ventilation towards those dependent lung\index{lung}
regions, thereby improving ventilation\index{ventilation}-perfusion\index{perfusion} matching. Dependent silent
spaces were significantly smaller when CPAP\index{CPAP} was applied compared to anesthesia\index{anesthesia}
without CPAP\index{CPAP}, and non-dependent silent spaces did not increase demonstrating
that CPAP\index{CPAP} leads to a decrease in atelectasis\index{atelectasis} without causing overdistension
\cite{Mosing2018Regional}.


\item {\bf Tidal volume}

The routine measurement of tidal\index{tidal} volume in anesthetized horses is not commonly
performed due to a lack of practical and affordable equipment for tidal\index{tidal} volumes
of up to 20 litres per breath. In some situations, spirometric\index{spirometry} information
would be highly desirable because capnography results can falsely suggest
adequate ventilation despite the presence of pronounced ventilation/perfusion
mismatch \cite{Mosing2018Physiologic}.
In one study in mechanically ventilated anesthetized horses, a very
close relationship between measured tidal\index{tidal} volume and impedance\index{impedance} change was found
for a wide range of tidal\index{tidal} volumes (4--10\,L per breath). This relationship was
further improved when total impedance\index{impedance} change in the entire image was calculated
compared to impedance\index{impedance} changes within predefined lung\index{lung} ROIs\index{ROI} especially at high
tidal\index{tidal} volumes. The authors concluded that during breaths with high tidal\index{tidal}
volumes the lungs become overinflated and extend over the ROI borders leading
to a loss of correct ``volume'' information when the FE model is used
\cite{Mosing2019Monitoring}. In a
clinical study the linear relationship between the total impedance\index{impedance} change and
measured tidal\index{tidal} volume was confirmed. Based on the individual line of best fit
the tidal\index{tidal} volume was estimated by impedance\index{impedance} measurement and was within 20\% of
the tidal\index{tidal} volume recorded using spirometry\index{spirometry} (Author's unpublished data).


\item {\bf Airflow}

Equine asthma\index{asthma}, also known as heaves, is a chronic performance-limiting disease
which affects horses of all ages and breeds. The disease is characterised by
bronchoconstriction\index{bronchoconstriction}, mucus production and smooth muscle remodelling, however
the clinical signs are nonspecific and can be subtle, which makes diagnostic
evaluation challenging, particularly when examining horses in the field. This
is also true for monitoring the effectiveness of treatment in severe cases
suffering from respiratory distress.

Global airflow signals in the lungs can be generated by calculating a smoothed first
derivative of the EIT volume signal. This has been used to demonstrate global
and regional ventilation characteristics related to changes in airway diameter
in people \cite{Ngo2018Flow}.

Changes in global and regional peak airflow calculated from the EIT signals
have been compared with a validated method (flowmetric plethysmography\index{plethysmography}) in
standing horses before and after histamine-induced bronchoconstriction\index{bronchoconstriction}
\cite{Secombe2020Evaluation}.
The expiratory peak flow calculated with EIT changed in
accordance with the flowmetric plethysmography\index{plethysmography} variable, particularly in the
ventral lung\index{lung} regions. 

In another study in horses, peak flows and flow-volume\index{flow-volume} curves were generated
from the EIT signal before and after inhalation of nebulized histamine. When
signs of airflow obstruction appeared, bronchodilation was obtained with
aerosolised salbutamol \cite{Secombe2019Bronchoconstriction}.
This study showed that EIT can
verify histamine-induced airflow changes and also subsequent reversal of these
changes by using a bronchodilator. EIT therefore has potential as an additional
method for pulmonary\index{pulmonary} function testing and as a non-invasive\index{non-invasive} monitoring tool to
guide therapy in horses with equine asthma\index{asthma}. 

The same concept of monitoring airflow with EIT was used to evaluate airflow
changes in healthy and asthmatic\index{asthma} horses before and after exercise when
respiratory rates reached pre-exercise levels \cite{Herteman2019Effect}.
Global and regional EIT gas
flow changes were seen after exercise in horses suffering from equine asthma\index{asthma}.
More pronounced flow changes were noted in the ventral dependent areas of
the lung\index{lung}, which is in accordance to the observed changes in the ventral lung\index{lung}
areas after histamine-induced bronchoconstriction\index{bronchoconstriction}.

\item {\bf Heart Rate}

Cardiovascular monitoring is essential in equine patients as the
anesthezia-related mortality is 1:100 horses even with basic anesthezia
monitoring used.
Measurement of heart rate is a standard parameter in monitoring anesthetized
patients. Therefore, every anesthetic monitor is expected to be able to
measure heart rate. A recent study showed an excellent agreement between EIT
and standard anesthetic monitor heart rate measurements when evaluating
impedance\index{impedance} changes of 4 pixels within the anatomical heart region \cite{Raisis2020Use}.
 The horses were positioned in dorsal recumbency during normotension,
hypotension and hypertension\index{hypertension}.

\item {\bf Conclusion}

To conclude, EIT has the potential to qualify as a new diagnostic tool in
equine medicine to evaluate lung\index{lung} pathology where routine diagnostic tools used
in other companion animals fail due to the size of the patient. This explains
why most clinical research on EIT in animals has focused on the equine species.
The scientific results to date have increased our knowledge of dynamic changes
in lung\index{lung} function occurring during anesthesia\index{anesthesia} or accompanying equine asthma\index{asthma}.
The EIT technology is only a step away from providing a novel practical way at
the stall side to monitor lung\index{lung} disease especially equine asthma\index{asthma} and follow up
treatment success. Including recent research EIT has the capability to be used
as a multiparameter monitor in anesthetized horses allowing the veterinarian
to follow tidal\index{tidal} volume changes and heart rate.

\end{enumerate}



\subsection{Dog}

Dogs are companion animals in close daily relationship with their owners. This
highlights the need for advanced medicine from complex surgeries to high level
life support and intensive care. Under these circumstances, where veterinarians
are often facing severe lung\index{lung} pathology, a respiratory monitor like the EIT
would support these patients the same way as it does aid human patients. Up to
the date of writing,
there are relatively few clinical research studies assessing EIT measurements
in dogs. This can be primarily related to the fact that good image quality is
more difficult to achieve. Dogs have longer fur than horses and therefore need
to be clipped when flat EIT electrodes\index{electrode} are used. Furthermore, their skin seems
to have a higher resistance to the current (possibly due to the lack of sweat
glands), making it difficult to collect good EIT raw data. Dog skin and
subcutis is highly mobile over the muscles which makes it difficult to have a
stable belt position and heart-related impedance\index{impedance} changes are very prominent in
the thoracic plane which provides maximum lung\index{lung} area (personal experience). The
optimal EIT belt position for clinical use has been determined based on
radiographic and CT images using ``fake'' EIT belts \cite{Rocchi2014Comparison},
and shown in \figref{fig:ch18:Dog-Xray}. The
sternum was used as an external landmark as this is palpable even in obese
dogs.

\begin{figure}[h]\centering
\includegraphics[width=0.6\columnwidth]{\CURRCHAP/fig-Dog-Xray/image3.png}
\FIGCAPTION{fig:ch18:Dog-Xray}{\CURRCHAP/fig-Dog-Xray}
\end{figure}


Two studies have been conducted in anaesthetised dogs evaluating the
distribution of ventilation using a 16 electrode EIT system (Pulmovista 500,
Dr\"ager, Germany).  One study evaluated the
effects of different PEEP\index{PEEP} levels in dogs in sternal (prone) position
\cite{Gloning2017Electrical},
while the other study looked at the change in the distribution of
ventilation before and after a recruitment manoeuvre using two different tidal\index{tidal}
volumes \cite{Ambrosio2017Ventilation}. In both studies, EIT was able to verify a change
in the CoV and ventilation in four regions of interest ordered from dependent
to non-dependent. Results were comparable with changes observed in CT images
and other measured functional ventilation variables, confirming the validity of
EIT to monitor distribution of ventilation in anaesthetised dogs.

Two other studies used EIT to monitor ventilation in sedated dogs. In both
studies, a 32 electrode EIT belt was used (BBVet, Sentec AG). In one study the
effect of the application of three CPAP\index{CPAP} interfaces on end-expiratory lung\index{lung}
impedance\index{impedance} (EELI\index{EELI}) was evaluated as a surrogate for functional residual\index{residual} capacity,
which is expected to increase under CPAP\index{CPAP} ventilation. Changes in thoracic
impedance\index{impedance} change ($\Delta Z$) were used as estimates of changes in tidal\index{tidal} volume 
\cite{Meira2018Comparison}.
Whereas all devices induced a significant increase in EELI\index{EELI} when CPAP\index{CPAP} was
applied, no difference between the interfaces was found in EIT variables.

Another study used EIT to monitor breathing pattern and changes in tidal\index{tidal}
volume following the application of two sedatives (dexmedetomidine and
medetomidine). Results show that both drugs decrease respiratory rate and
increase tidal\index{tidal} volume ($\Delta Z_{TV}$) suggesting that minute ventilation remains
essentially unchanged \cite{Pleyers2020Investigation}.

\subsection{Rhinoceros}


Rhinoceroses are anaesthetised for different purposes among which are
relocation procedures to bring them to safer areas away from poachers. This
``life-saving'' procedure invariably  has a profound negative impact on gas
exchange characterised by severe hypoxemia and hypercapnia
\cite{Radcliffe2014Pulmonary}. The
pathophysiology of these severe complications is still incompletely understood
as monitoring due to the size of the animal and to the fact that they are
immobilised in the field is difficult.  Therefore, EIT measurements were
considered on several occasions and successfully performed making use of
battery power, a custom made extra-long EIT belt and sufficient electrical
current generation at the electrode/skin interfaces. Rhinoceroses are the
largest mammals where EIT signal collection has been performed and  this proved
the use of EIT as a versatile in the field monitoring and research device
(\figref{fig:ch18:animals}D).

\begin{figure}[h]\centering
\includegraphics[width=1.0\columnwidth]{\CURRCHAP/fig-Animals/animals.jpg}
\FIGCAPTION{fig:ch18:animals}{\CURRCHAP/fig-Animals}
\end{figure}

In a study in immobilized laterally recumbent rhinoceroses pronounced shifts in
ventilation\index{ventilation} and perfusion\index{perfusion} towards the non-dependent lung\index{lung} were demonstrated. The
EIT images including perfusion\index{perfusion} mapping after injection of hypertonic\index{hypertonic} saline
showed that the dependent lung\index{lung} did not collapse but stayed aerated\index{aeration} while only
minimally perfused. The decrease in perfusion\index{perfusion} is very likely due to hypoxic
pulmonary\index{pulmonary} vasoconstriction. Breath holding in some rhinoceroses shifted
ventilation towards the dependent lung\index{lung} \cite{Mosing2020What}.

The gas exchange is worse in lateral compared to sternal recumbency. EIT has
been successfully used to compare distribution of ventilation in anaesthetised
rhinoceroses in lateral and sternal recumbency \cite{Mosing2017Distribution}.
A large difference in
ventilation between the two lungs was observed. As in the first study the
non-dependent lung\index{lung} contributed 80\% of the total $\Delta Z$ in lateral recumbency, while
the dependent lung\index{lung} was only minimally ventilated. In sternal recumbency the
difference in ventilation between the two lungs was only 10\%. These findings
show that the impairment in gas exchange in anaesthetised lateral recumbent
rhinos is likely due to marked ventilation\index{ventilation}-perfusion\index{perfusion} mismatch, rather than an
increase in pulmonary\index{pulmonary} dead space, as previously suggested. It also highlights
the potential of EIT to elucidate lung\index{lung} pathophysiology non-invasively in
animals that normally cannot be monitored due to the fact that they are living
in the wild or are too big for all other known monitoring devices.

\section{
Clinical Applications in Animals 
}

To date, EIT has not been used extensively as a routine monitoring tool to
manage clinical situations. One of the reasons might be that the displayed
variables do not yet represent the information the clinician needs to change
case management.
Reports on the use of EIT in clinical cases are mostly anecdotal.
However, some case reports and case studies can be found in the veterinary
literature. 

\begin{enumerate}
\item {\bf Horse}

A case study with six horses endorses the potential usefulness of EIT as a
diagnostic imaging technique in large animal species \cite{Herteman2019Distribution}.
Two healthy horses, one with exercise-induced pulmonary\index{pulmonary} hemorrhage\index{hemorrhage} (EIPH), two
with pleuropneumonia and one with cardiogenic pulmonary\index{pulmonary} edema\index{edema} (CPE) were
included. EIT was recorded in these standing non-sedated horses over 10 breath
cycles and the distribution of ventilation was evaluated (CoV and silent
spaces).
 In the horses with lung\index{lung} disease, a shift of CoV towards the non-dependent lung\index{lung}
fields/segments and an increase in dependent silent spaces was demonstrated
compared to healthy horses. EIT was thus able to detect changes in the
distribution of ventilation as well as the presence of poorly ventilated lung\index{lung}
units in horses with naturally occurring pulmonary\index{pulmonary} pathology.

In a separate study EIT data from horses suffering from left sided cardiac\index{cardiac}
volume over load disease and therefore different degrees of pulmonary\index{pulmonary} edema\index{edema}
were collected and compared to a healthy cohort of horses (unpublished data). A
distinct dorsal shift of the ventilation with loss of ventilated lung\index{lung} area in
the ventral parts of the lungs was observed. The EIT also revealed that the
left lung\index{lung} is more effected by the volume overload disease. Specific EIT
variables describing the distribution of ventilation were able to distinct
between healthy, subclinical and clinical cases of cardiac\index{cardiac} volume overload. 

\item {\bf Orangutan}

A case report describing the clinical use of EIT in an orangutan with recurrent
airway disease undergoing diagnostic evaluation demonstrated how EIT can help
at the bedside\index{bedside} to detect ventilatory issues quickly and allow goal-directed
treatment \cite{Mosing2017Ventilatory}. A belt designed for humans (Sentec,
Switzerland) was placed directly after induction of anaesthesia to continuously
monitor ventilation during planned transport between different locations within
the hospital (radiology, CT imaging, and bronchoscopy room, 
\figref{fig:ch18:animals}C).
 Immediately after
endotracheal intubation one-lung\index{lung} intubation and ventilation became apparent on
the EIT. The endotracheal tube was withdrawn until both lungs were visibly
ventilated on the EIT images. Nevertheless, one lung\index{lung} continued to show reduced
regional stretch and large silent space which was subsequently verified as a
large pathologic process (abscess) in the lung\index{lung} on CT.
After a diagnostic  bronchoscopy procedure , EIT revealed increased silent
spaces  now in the peripheral lung\index{lung} area,  which were indicative of excess
residual lavage fluid within the lung\index{lung} tissue. Subsequent weaning from the
ventilator was guided by improvements in lung\index{lung} function as  suggested by the EIT
image.
In this case EIT
played a key role in safe anaesthesia monitoring and non-invasive\index{non-invasive} diagnostics.

\end{enumerate}


\section{%
Future of EIT in veterinary applications
}

As EIT becomes established as a useful non-invasive\index{non-invasive} and affordable method for
dynamic general and regional lung\index{lung} function examination in the veterinary field,
this new technology is expected to be increasingly used for a broader range of
indications, conditions and animal species over the coming years. 

The authors of this book chapter have collected data from cows, sheep, pigs,
dogs, calves, foals, rhinoceroses, an orangutan and even birds evaluating
anaesthesia- and ventilatory-related variables (\figref{fig:ch18:animals}).
  New applications are currently
being investigated. Preliminary data on the evaluation of heart rate in
standing animals based on thoracic EIT measurements look very promising. Also
evaluation of lung\index{lung} disease over time in cattle which has a big economical
impact in the agricultural industry is on its way. The equine racing industry
is more and more interested in early diagnosis of lung\index{lung} diseases effecting
athletic horses like exercise induced pulmonary\index{pulmonary} hemorrhage\index{hemorrhage}, but also common
lung\index{lung} diseases in foals. EIT is the most promising new technology that can
fulfil those requirements.The option of heart rate measurement in addition to
the ventilation signal would further improve the clinical usefulness of EIT in
veterinary applications.

EIT has a bright future in veterinary medicine as a non-invasive\index{non-invasive} respiratory
monitoring tool that can be used at the stable-side and in the field with
minimal time effort and impact on the animals health and welfare.






\ifdefined\BUILDCHAPTERALONE
 \bibliography{../bib/book} \bibliographystyle{ieeetr}
 \end{document}
\fi
