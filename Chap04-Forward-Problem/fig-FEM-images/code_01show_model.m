SSMM = [3,4,3,4];
contours = linspace(-0.1,0.1,99);
pp = struct();
optx.edge.width=0.5;
optx.edge.color=0.3*[1,1,1];
optx.viewpoint = struct('az',10,'el',10);
optx.edge.significant.viewpoint_dependent.callback = false;
x_pts = linspace(-5,10,301); y_pts = linspace(20,30,201);

for fn=2:2; switch fn
   case 1; pp.zCUT =  1.5;
           cm=-1000; clrm=-5;
           opt = optx;
           opt.electrode.number.show= true;
           opt.electrode.number.font_size= 12;
   case 2; pp.zCUT = -0.1;
           cm=-8.5; clrm=-5;
           opt = optx;
   case 3; pp.zCUT =  1.5;
           cm=-1000; clrm=1000;
           opt = optx;
           opt.electrode.number.show= true;
           opt.electrode.number.font_size= 30;

   end

   img = fem_model_arms( pp) ;
   img.fwd_model.nodes = img.fwd_model.nodes *100; % cm
   img.calc_colours.clim= 0.5;
   imgc = crop_model(img,@(x,y,z) y<cm & z>pp.zCUT*50+2 & x>-4);
   xyz = interp_mesh(imgc);
   imgc.elem_data( xyz(:,2)<clrm ) = imgc.calc_colours.ref_level;
%show_fem(imgc); view(- 0,0);
   show_fem_enhanced(imgc,opt)
   axis off
   print_convert(sprintf('%s-f%d.jpg',mfilename,fn));
  
   zlim([20,30]); xlim([-10,0])
   print_convert(sprintf('%s-z%d.jpg',mfilename,fn));
end
