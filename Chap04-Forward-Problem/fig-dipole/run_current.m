xls = linspace(-4,4,201);
yls = linspace(-1.0,1.0,101);
[x,y]= meshgrid(xls,yls);
V = 1./sqrt((x-1).^2 + y.^2) - 1./sqrt((x+1).^2 + y.^2);
c = 3*linspace(-1,1,23); c=abs(c).^2.5.*sign(c);
contour(xls,yls,V, c,'Color',[0,0,0]);
axis off; axis equal
print -dpdf figure.pdf
