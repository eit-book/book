clf
ci = [5 .5 .1 .05]; 
%% EDIT show_fem.m line 256
%      line(vx+ctr_x,vy+ctr_y,  ...
%           'LineWidth', 8 ...
for i=1:4
   img.fwd_model.electrode(1).z_contact=ci(i);
   vh = fwd_solve(img);
   imgc.fwd_model.mdl_slice_mapper.xpts = linspace(-0.25,0.25,200);
   imgc.fwd_model.mdl_slice_mapper.ypts = linspace(0.8,1,100);
   q = show_current(imgc,vh.volt);
   hh=show_fem(imgc);
   set(hh,'EdgeColor',[1,1,1]*.50);
   hold on;

   sy = linspace(.98,.70,20); sx= 0*sy - 0.25;
   hh=streamline(q.xp,q.yp, q.xc, q.yc, sx,sy); set(hh,'Linewidth',2);
   hh=streamline(q.xp,q.yp,-q.xc,-q.yc,-sx,sy); set(hh,'Linewidth',2);

%  title(sprintf('streamlines zc = %5.3f',fmdl.electrode(1).z_contact));
   hold off;
   axis([-.25,.25,0.75,1.02]);
   axis off

%  title(sprintf('current near electrode:  zc = %5.3f',ci(i)));
   print_convert(sprintf('contact_impedance04%c.png','a'+i-1));
end
