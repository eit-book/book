clf
ci = [5 .5 .1 .05]; 
fmdl = img.fwd_model;
fel  = fmdl.elems;
fx   = interp_mesh(fmdl,0); fx=fx(:,1);
for i=1:4
   fmdl.electrode(1).z_contact=ci(i);
   vh = fwd_solve(img);
   ch = calc_elem_current(img);
   ch= sqrt(sum(ch.^2,2));

   e1n = fmdl.electrode(1).nodes;
   feb=find_electrode_bdy(fmdl.boundary, fmdl.nodes, e1n);
   
   try; clear xp,cp; end
   for j=1:length(feb)
     bj = fmdl.boundary(feb(j),:);
     ej = find( any(fel==bj(1),2) & ...
                any(fel==bj(2),2));
     xp(j) = fx(ej);  %xposn of elem
     cp(j) = ch(ej);  %current into node
   end
   [~,idx]= sort(xp);
   cp = cp(idx);
   feb= feb(idx);
   xp = sort(unique(fmdl.nodes( ...
        fmdl.boundary(feb,:),1))); 

   pts = interp1( cumsum([0,cp]), xp,  ...
           linspace(0,sum(cp),20));
   
   
   imgc.fwd_model.mdl_slice_mapper.xpts = linspace(-0.25,0.25,200);
   imgc.fwd_model.mdl_slice_mapper.ypts = linspace(0.8,1,100);
   q = show_current(imgc,vh.volt);
   hh=show_fem(imgc);
   set(hh,'EdgeColor',[1,1,1]*.50);
   hold on;

%  sx = linspace(-.1, .1 ,20);
   sx = pts-min(pts); sx=sx/max(sx); sx= 0.2*sx-0.1;
   sy= sqrt(1-sx.^2);
   hh=streamline(q.xp,q.yp,-q.xc,-q.yc, sx,sy); set(hh,'Linewidth',2);

   hold off;
   axis([-.15,.15,0.85,1.02]);
   axis off

%  title(sprintf('current near electrode:  zc = %5.3f',ci(i)));
   print_convert(sprintf('contact_impedance07%c.png','a'+i-1));
%  print(sprintf('contact_impedance07%c.pdf','a'+i-1));
end
