[imdl,img]= human_imdl;
clf
fname = 'DATA/STUDYNAME/SUBJECT_1/YYYYMMDD/Eit/Viasys/1001_c4.get';
vv= eidors_readdata(fname);
vr = vv(:,56);
img= inv_solve(imdl, vr, vv);

imgs= -calc_slices(img); % Negative to air is +

img.calc_colours.ref_level=0;
img.calc_colours.greylev=.01;
img.calc_colours.backgnd=[hex2dec('ff'),hex2dec('fb'),hex2dec('db')]/255;
img.elem_data = img.elem_data(:,74:4:120);
imgc = calc_slices(img);
imgc([1,63],:,:)= 0; imgc(:,[1,63],:)= 0;

cla; hold on;
[yy,zz]=meshgrid(1:size(imgc,2),1:size(imgc,1));
for i = 1:size(imgc,3);
   frac= 1 + (i/size(imgc,3))*.2;
   ci = calc_colours(imgc(:,:,i), img);
%  ci(ci==1)= NaN;
   xx =  40*i + zeros(size(ci));
   hh(i) = surf(frac*xx,-frac*(yy)-5*i,-frac*zz-0.5*i,ci);
   set(hh(i),'EdgeAlpha',0);
end
view(90,-75);
hold off; axis off;

 print_convert analyse_step02a.jpg

clf; subplot(211);
img.get_img_data.frame_select = 01;
show_slices(img);
print_convert analyse_step02b.jpg
