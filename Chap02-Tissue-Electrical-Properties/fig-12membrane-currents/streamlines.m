shape_str = [ ...
  'solid top = plane(0, 5,0;0, 1,0);' ...
  'solid bot = plane(0,-5,0;0,-1,0);' ...
  'solid srf = plane(0, 0,0;0, 0,1) -maxh=0.4;' ...
            ];
xy = [0,   0;  2.2,   0;  4.4,   0; -2.2,   0; -4.4,   0;
    3.3, 1.9;  1.1, 1.9; -1.1, 1.9; -3.3, 1.9;  3.3,-1.9;
    1.1,-1.9; -1.1,-1.9; -3.3,-1.9];
for cell = 1:size(xy,1)
    posn = cell*ones(1,13);
    posn(2:3) = xy(cell,:); posn(5:6) = xy(cell,:);
    shape_str = sprintf([ shape_str,  ...
      'solid celli%d = sphere(%f,%f,0;0.9);' ...
      'solid cellm%d = sphere(%f,%f,0;1) -maxh=0.1;' ...
      'solid celli%d_= celli%d and srf;' ...
      'solid cellm%d_= cellm%d and (not celli%d) and srf;' ...
      'tlo   celli%d_; tlo cellm%d_;', ...
                        ], posn);
    end
cello = join(sprintfc('cellm%d',1:cell),' or ');
shape_str = [ shape_str, ...
  'solid cello = ',cello{1},';' ...
  'solid ob =  orthobrick(-5,-9,-.5;5,9,0) and not cello;' ...
  'solid mainobj= ob and top and bot and srf;' ...
            ];
elec_pos = [ 0,5,0,0,1,0; 0,-5,0,0,-1,0];
elec_shape=[6.0];
elec_obj = {'top','bot'};
fmdl = ng_mk_gen_models(shape_str, elec_pos, elec_shape, elec_obj);
fmdl.stimulation = stim_meas_list([1,2,1,2],2);
fmdl = mdl2d_from3d(fmdl);
img = mk_image(fmdl,1);
img.calc_colours = struct('ref_level',1,'clim',1.5);
mi = vertcat( fmdl.mat_idx{2*(1:size(xy,1))} );
ls = linspace(-4,4,101);
msm = struct('level',[inf,inf,-0.1],'x_pts', ls, 'y_pts', ls);
img.fwd_model.mdl_slice_mapper = msm;
sx = linspace(-4,4,21); sy= 4.0 + 0*sx;

for sp = 1:3; switch sp
    case 1; c= 0.1;
    case 2; c= 0.03;
    case 3; c= 0.01;
    end
    subplot(1,3,sp);
    img.elem_data(mi) = c;
    q = show_current(img);
    hh= show_fem(img); set(hh,'EdgeAlpha',0.1); view(2)
    hh=streamline(q.xp,q.yp,q.xc,q.yc,sx,sy);
    set(hh,'Linewidth',3, 'color',[148,43,77]/255)
    xlim(2.1*[-1,1]); ylim(4*[-1,1]); axis off
    title(sprintf('\\sigma^*_m = %3.2f',c))  
end
set(gcf,'Renderer','Painters')
print('-dpdf',[mfilename,'.pdf'])

