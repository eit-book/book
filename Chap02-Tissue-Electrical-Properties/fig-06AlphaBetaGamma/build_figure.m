clf; subplot(3,1,[1,2])
lt = {'LineWidth',2};
%frequency 
% L=200000;
% dt=1e-9;
% Fs=1/dt;
% % T=0:dt:2;
% % T=T(1:L);
% f=Fs*(0:(L/2))/L;
lf=linspace(0,11,1000);
f=10.^lf;

%spleen specific
e0=8.85e-12;
einf_spleen=4;
tau_spleen=[7.96e-12 63.66e-9 256.26e-6 6.366e-3];
deltae_spleen=[48 2500 2e5 5e7];
alpha_spleen=[0.1 0.15 0.25 0];
sigma0_spleen=0.03;
e_spleen=einf_spleen;


for i=1:4
    e_spleen=e_spleen+deltae_spleen(i)./(1+1i*2*pi*f.*tau_spleen(i)).^(1-alpha_spleen(i));
end
e_spleen=e_spleen+sigma0_spleen./(1i*2*pi*f*e0);
permrel_spleen=real(e_spleen);
cond_spleen=imag(-e_spleen)*2*pi.*f*e0;
condperc_spleen=(cond_spleen-cond_spleen(2))./cond_spleen(2)*100;
permrelperc_spleen=(permrel_spleen-permrel_spleen(2))/permrel_spleen(2)*100;


%muscle specific
e0=8.85e-12;
einf_muscle=4;
tau_muscle=[7.23e-12 353.68e-9 318.31e-6 2.274e-3];
deltae_muscle=[50 7000 1.2e6 2.5e7];
alpha_muscle=[0.1 0.1 0.1 0];
sigma0_muscle=0.2;
e_muscle=einf_muscle;

for i=1:4
    e_muscle=e_muscle+deltae_muscle(i)./(1+1i*2*pi*f.*tau_muscle(i)).^(1-alpha_muscle(i));
end
e_muscle=e_muscle+sigma0_muscle./(1i*2*pi*f*e0);
permrel_muscle=real(e_muscle);
cond_muscle=imag(-e_muscle)*2*pi.*f*e0;
condperc_muscle=(cond_muscle-cond_muscle(2))./cond_muscle(2)*100;
permrelperc_muscle=(permrel_muscle-permrel_muscle(2))/permrel_muscle(2)*100;

% subplot(1,2,1);
loglog(f,permrel_muscle,'b',lt{:});
hold on;
%loglog(f,permrel_spleen,'b--');
set(gca,'FontSize',16);
mystr=['Relative permittivity ',char(949),'^{''''}_r'];
ylabel(mystr);
%hold on;
yyaxis right
endcond=find(abs(lf-9)<5e-3);
loglog(f(1:endcond(1)),cond_muscle(1:endcond(1)),'r',lt{:});
%loglog(f(1:endcond(1)),cond_spleen(1:endcond(1)),'r--');

ylabel('Conductivity, S/m');
%title('Muscle');
xlabel('Frequency (Hz)');
%legend('Muscle Permittivity','Spleen Permittivity','Muscle Conductivity','Spleen Conductivity');

% subplot(1,2,2);
% loglog(f,permrel_spleen);
% 
% hold on;
% yyaxis right
% 
% loglog(f,cond_spleen,'r');
% title('Spleen');
box off
print -dpdf figure.pdf
