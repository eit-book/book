%calculation of conductivities of saline
clear

concpercent=[0.5 1 2 5 10 15 20 25];%ms/Cm
concpercentfine=[0.5 0.6 0.7 0.8:0.01:1 1.01:0.01:10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25];
molarmass=58.44; %g
concgl=concpercent*10;%x10 to convert from g/100ml
concglfine=concpercentfine*10;
molarity=concgl/molarmass;%x10 to convert to g/l and then divide by molar mass g/mol
molarityfine=concglfine/molarmass;
tablecond=[8.2 16 30.2 70.1 126 171 204 222]/10;%original in mS/cm. Divide by 10 to get to S/m
%concpercent, concgl, molarity matches with cond

%estimate conductivity from infinite dilution Debye-Hückel Onsager rule

Lambda0=126.39e-4; %ohm m2 mol-1
A=60.20e-4; %ohm m2 mol-1
B=0.229; %mol^0.5/l^1.5

Lambda=Lambda0-(A+B*Lambda0)*molarityfine.^0.5;
condDHO=Lambda.*molarityfine*1000;

newmolarity=[5e-4 1e-3 5e-3 0.01 0.02 0.05 0.1];
tableLambda=[124.44 123.68 120.59 118.45 115.70 111.01 106.69]*1e-4;
newtablecondDHO=tableLambda.*newmolarity*1000;

newLambda=Lambda0-(A+B*Lambda0)*newmolarity.^0.5;
checkcondDHO=newLambda.*newmolarity*1000;

% figure;
% loglog(molarity,1./tablecond);
% hold on;
% loglog(newmolarity,1./newtablecondDHO,'r')

%figure;
subplot(3,1,[1,2])
lt = {'LineWidth',2};
loglog(molarity,tablecond,'b',lt{:});
set(gca,'FontSize',14);
hold on;
%loglog(newmolarity,newtablecondDHO,'r');
loglog(molarityfine,condDHO,'r',lt{:})
loglog(newmolarity,checkcondDHO,'r',lt{:});
ylim([1e-3,1e2]);xlim([1e-4,1e1]);
legend({'Measured Conductivity','Predicted Conductivity'},'Location','northwest');
ylabel('Conductivity (S/m)');
xlabel('Concentration of NaCl (M)');
box off; print -dpdf figure.pdf
