clf;
FS = 12;
lt = {'LineWidth',2,'MarkerSize',10};
e0=8.85e-12;% F-1m-1

freqexp=0:.1:4;
freq=10.^freqexp;
omega=2*pi*10.^freqexp;
geofactor=20; %A/d

einf=4;
tau1=8.38e-12;
tau2=130e-9;
%tau3=150e-6;%from transition 3 of Gabriel 96c

deltae2=5200;
deltae1=56;
alpha2=0.1;
alpha1=0.1;

deltae=10000;
tau=1e-3;
alpha=0.3;

epsilon=einf+deltae./(1+(1i*omega*tau).^(1-alpha));

Y=1i*omega.*geofactor.*epsilon*e0;
Z=1./Y;
R=real(Z);
X=imag(Z);

subplot(2,2,1);
semilogx(freq,real(epsilon),lt{:});
set(gca,'Fontsize',FS);
xlabel('Frequency (Hz)');
mystr=[char(949),'^''_r'];
ylabel(mystr);
hold on;
yyaxis right
semilogx(freq,-imag(epsilon),lt{:});
mystr=[char(949),'^{''''}_r'];
ylabel(mystr,'rotation',270);
box off
ytl = get(gca,'YTickLabel');
ytl{3} = '';
set(gca,'YTickLabel',ytl);


subplot(2,2,2);
semilogx(freq,-imag(epsilon).*omega*e0,'b',lt{:});
set(gca,'Fontsize',FS);
xlabel('Frequency (Hz)');
ylabel('Conductivity (S/m)');
yyaxis right
semilogx(freq,real(epsilon),lt{:});
mystr=[char(949),'^''_r'];
ylabel(mystr,'rotation',270);
box off

subplot(2,2,3);
plot(real(epsilon),-imag(epsilon),lt{:});
set(gca,'Fontsize',FS);
hold on;
plot(real(epsilon(11:10:31)),-imag(epsilon(11:10:31)),'x',lt{:});
labels=cellstr(num2str(transpose(freq(11:10:31))));
labels=strcat(labels,' Hz');
text(real(epsilon(11:10:31)),-imag(epsilon(11:10:31)),labels,'VerticalAlignment','bottom','HorizontalAlignment','left','Fontsize',14);
mystr=[char(949),'^''_r'];
xlabel(mystr);
mystr=['-i',char(949),'^{''''}_r'];
ylabel(mystr);
ylim([0 4000]);

box off

subplot(2,2,4);
plot(R,-X,lt{:});
set(gca,'Fontsize',FS);
hold on;
plot(R(11:10:31),-X(11:10:31),'x',lt{:});
%labels=cellstr(num2str(transpose(freq(11:10:31))));
text(R(21:10:31)-20,-X(21:10:31),labels(2:3),'VerticalAlignment','bottom','HorizontalAlignment','left','Fontsize',14);
ylim([0 4000]);
xlabel('R_z (\Omega)');
ylabel('-X_z (\Omega)');

box off
print -dpdf figure.pdf
