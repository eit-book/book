clear
FS = 12;
lt = {'LineWidth',2};
clf;
freqexp=0:.1:4;
freq=10.^freqexp;
omega=2*pi*10.^freqexp;
geofactor=20; %A/d
R=500;
% C=geofactor*er*e0;
e0=8.85e-12;% F-1m-1
eprimeinfr=1000;
deltaeprime=10000;
deltaC=deltaeprime*e0*geofactor;
tau=R*deltaC;
eprimeinf=eprimeinfr*e0;
eprime=eprimeinf+deltaeprime./(1+omega.^2*tau.^2);
sigmadc=1e-10;
epprimedc=sigmadc./(omega*e0);
epprime=deltaeprime*tau*omega./(1+omega.^2*tau.^2);
epprimemod=epprime+epprimedc;

subplot(2,2,1);
semilogx(freq,eprime,lt{:})
set(gca,'Fontsize',FS);
mystr=[char(949),'^{''}_r'];
ylabel(mystr);
hold on;
yyaxis right
semilogx(freq,epprimemod,lt{:});
xlabel('Frequency (Hz)');
mystr=[char(949),'^{''''}_r'];
ylabel(mystr,'rotation',270);
box off

%alternative
deltasigmap=1/R*geofactor;
sigmap=sigmadc+deltasigmap*omega.^2*tau^2./(1+omega.^2*tau.^2);
subplot(2,2,2);
semilogx(freq,sigmap,lt{:});
set(gca,'Fontsize',FS);
xlabel('Frequency (Hz)');
ylabel('Conductivity (S/m)');
hold on;
yyaxis right
semilogx(freq,eprime,lt{:});
mystr=[char(949),'^{''}'];
ylabel(mystr,'rotation',270);
set(gca,'YTick',[0:2.5:10]*1e3)
ytl = get(gca,'YTicklabel');
ytl{3} = '';
set(gca,'YTicklabel',ytl)
box off

%alternative
subplot(2,2,3);
plot(eprime,epprime,lt{:});
set(gca,'Fontsize',FS);
mystr=[char(949),'^{''}_r'];
xlabel(mystr);
mystr=[char(949),'^{''''}_r'];
ylabel(mystr); 
hold on;
plot(eprime(11:10:31),epprime(11:10:31),'x');
labels=cellstr(num2str(transpose(freq(11:10:31))));
labels=strcat(labels,' Hz');

text(eprime(11:10:31),epprime(11:10:31),labels,'VerticalAlignment','bottom','HorizontalAlignment','left','Fontsize',14);
%xlim([-2.5 22.5]);
%ylim([0 12.5]);
box off

%alternative
subplot(2,2,4);
%resistance and reactance Wessel diagram
Cinf=geofactor*e0*eprimeinfr;
taumod=R*Cinf;
measuredR=tau*(deltaC)./(omega.^2*tau.^2*Cinf^2+(Cinf+deltaC).^2);
measuredX=-(omega.^3*tau^2*Cinf+omega.*(Cinf+deltaC))./(omega.^4*tau.^2*Cinf^2+omega.^2*(Cinf+deltaC).^2);
measuredRsimple=R./(1+omega.^2*taumod.^2)+sigmadc/geofactor;
measuredXsimple=-(omega.*taumod*R)./(1+omega.^2*taumod.^2);

plot(measuredR,-measuredX,lt{:});
set(gca,'Fontsize',FS);
hold on;
plot(measuredR(11:10:31),-measuredX(11:10:31),'x');
%labels=cellstr(num2str(transpose(freq(1:10:21))));
text(measuredR(11:10:31),-measuredX(11:10:31),labels,'VerticalAlignment','top','HorizontalAlignment','left','Fontsize',14);
xlabel('R (\Omega)');
ylabel('-iX (\Omega)');
ylim([0 2000]);
xlim([0 500]);
box off
print -dpdf figure.pdf
