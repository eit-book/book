clf
lt = {'LineWidth',2};
load fig11data dataObjs;
%filename='fig-11anisotropy_RHS.fig';
%fig = openfig(filename); % Open figure and assign it to fig object
%dataObjs = findobj(fig,'-property','YData'); % Find all graphic objects with YData, in our case line values
nlines=length(dataObjs);

for i=10:-1:1
    obj=dataObjs(i);
    legend_name{i,1} = dataObjs(11-i).DisplayName;
    datastrbeg=strcat('dataObjs(',num2str(i),')');
    xdatastr=strcat('tempxdata=',datastrbeg,'.XData;');
    ydatastr=strcat('tempydata=',datastrbeg,'.YData;');
    eval(xdatastr);
    eval(ydatastr);
    loglog(tempxdata,tempydata,'Color',obj.Color,'LineStyle',obj.LineStyle,'LineWidth',obj.LineWidth,'Marker',obj.Marker,'MarkerSize',obj.MarkerSize,lt{:});
    hold on;
end
legend(legend_name,'Location','west');
xlim([1 250]);
ylim([1e-7 1]);
xlabel('\sigma_t/\sigma_r','fontsize',20);
ylabel('\rho_{app}/\rho_r','fontsize',20);
box off
print -dpdf figure.pdf
